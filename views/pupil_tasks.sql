create or replace view pupil_task as (
with prepared as (
select pupil.id, name, surname,
       count(*) as num_attempts,
       sum(cast(result = 'OK' as int)) as num_OK
from submission inner join pupil on submission.pupil_id = pupil.id
group by pupil.id order by num_OK desc
)
select *, cast(num_OK as float)/num_attempts as accuracy from prepared
);

-- select * from pupil_task;
