create or replace view pupil_grade as (
select pupil.id, name, surname,
       avg(score) as average,
       count(score) as num_grades
from grade inner join pupil on grade.pupil_id = pupil.id
where date_to > now() group by pupil.id order by average desc
);

select * from pupil_grade;
