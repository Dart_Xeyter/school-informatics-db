create or replace view task_submission as (
with total as (
select contest_num, task_num, result, time,
       count(*) over (partition by contest_num, task_num)
from submission
)
select contest_num, task_num, count as num_attempts,
       count(*) as num_OK, min(time)/1000.0 as fastest_solve
from total where result = 'OK'
group by contest_num, task_num, num_attempts

union

select contest_num, task_num, count as num_attempts,
       0 as num_OK, cast(null as float) as fastest_solve
from total where not exists(select * from total as t where result = 'OK' and
    (t.contest_num, t.task_num) = (total.contest_num, total.task_num))
group by contest_num, task_num, num_attempts

order by contest_num, task_num
);

-- select * from task_submission;
