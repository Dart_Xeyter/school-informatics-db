create or replace function statements_dif(id_ int,
    out was text, out now text) as $$
declare
    cur text = (select cur from statement where id = id_);
    prev text = (select prev from statement where id = id_);
    min_len int = least(length(cur), length(prev));
    start_ind int = 0;
    end_ind int = 0;
begin
    if cur is null or prev is null then
        was = prev;
        now = cur;
    else
        while start_ind < min_len and substr(prev, start_ind, 1) =
                                      substr(cur, start_ind, 1) loop
            start_ind = start_ind+1;
        end loop;
        if start_ind != min_len then
            while end_ind < min_len and
                    substr(prev, length(prev)-end_ind-1, 1) =
                    substr(cur, length(cur)-end_ind-1, 1) loop
                end_ind = end_ind+1;
            end loop;
        end if;
        was = substr(prev, start_ind, length(prev)-end_ind-start_ind);
        now = substr(cur, start_ind, length(cur)-end_ind-start_ind);
    end if;
end
$$ language plpgsql;

-- insert into statement values (137, 'mama\nhoho\n1', 'mama\nhehehe\n1');
-- insert into statement values (138, 'mama\nhoho\n1', 'mama\nhoho\n1');
-- select * from statement;
--
-- select * from statements_dif(138);
