create or replace function shortest_solve(
    contest_id int, task_id text) returns text as $$
declare
    prog text;
    ans text;
    len int = 1e9;
begin
    for prog in (select program from submission natural join code
        where result = 'OK' and contest_num = contest_id and task_num = task_id
    ) loop
        if length(prog) < len then
            ans = prog;
            len = length(ans);
        end if;
    end loop;
    return ans;
end
$$ language plpgsql;

select * from shortest_solve(2, 'I');
