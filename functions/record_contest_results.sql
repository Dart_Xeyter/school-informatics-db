create or replace procedure record_results(num int, n int) as $$
declare
    ending date = (select deadline from contest where num = number);
    num_OK int;
    mark float;
    student int;
begin
    if ending is null or ending > now() then
        raise exception 'Contest hasn''t finished';
    end if;
    for student in (select id from pupil) loop
        num_OK = (select count(*) from submission where result = 'OK' and
                  pupil_id = student and contest_num = num);
        mark = greatest(2, least(5, round(5.0*num_OK/n)));
        insert into grade values (default, student, mark, num, default, default);
    end loop;
end
$$ language plpgsql;

-- call record_results(9, 1);x
--
-- select * from submission
-- where contest_num = 9 and result = 'OK';
--
-- select * from contest;
-- select * from grade;
