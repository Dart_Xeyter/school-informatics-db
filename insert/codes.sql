insert into code values
(default, 'python', 'n = int(input())
back_a = [0 for i in range(n)]
back_other = [0 for j in range(n)]
back_a[0] = 1
back_other[0] = 2
for u in range(1, n):
    back_a[u] = back_other[u - 1]
    back_other[u] = 2 * (back_a[u - 1] + back_other[u - 1])
print((back_a[-1] + back_other[-1]) % (10 ** 9 + 7))'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n, mod = (int)(pow(10, 9)) + 7; cin >> n;
    vector<int> back_zero(n), back_one(n);
    back_zero[0] = 1;
    back_one[0] = 1;
    for (int i = 1; i < n; ++i) {
        back_zero[i] = (back_zero[i - 1] + back_one[i - 1]) % mod;
        back_one[i] = back_zero[i - 1];
    }
    cout << (back_zero[n - 1] + back_one[n - 1]) % mod << "\n";
    return 0;
}'),
(default, 'g++', '#include <bits/stdc++.h>
#define int long long

using namespace std;

int32_t main()
{
    int n; cin >> n;
    vector<int> dp(n);
    dp[0] = 1;
    if (n == 1) {
        cout << dp[n - 1];
    }
    else if (n == 2) {
        cout << 2;
    }
    else if (n == 3) {
        cout << 4;
    }
    else {
        dp[1] = 2;
        dp[2] = 4;
        for (int i = 3; i < n; ++i) {
            dp[i] = dp[i - 1] + dp[i - 2] + dp[i  - 3];
        }
        cout << dp[n - 1];
    }
    return 0;
}'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, m; cin >> n >> m;
    vector<vector<int>> a(n, vector<int>(m)), dp(n, vector<int>(m));
    vector<char> dp2;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> a[i][j];
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (i == 0 && j == 0) {
                dp[i][j] = a[i][j];
            }
            else if (i != 0 && j == 0) {
                dp[i][j] = dp[i - 1][j] + a[i][j];
            }
            else if (i == 0) {
                dp[i][j] = dp[i][j - 1] + a[i][j];
            }
            else {
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + a[i][j];
            }
        }
    }
    int i = n - 1, j = m - 1;
    while (i != 0 || j != 0) {
        if (i == 0) {
            dp2.push_back(''R'');
            j--;
        }
        else if (j == 0) {
            dp2.push_back(''D'');
            i--;
        }
        else if (dp[i - 1][j] >= dp[i][j - 1]) {
            dp2.push_back(''D'');
            i--;
        }
        else if (dp[i - 1][j] < dp[i][j - 1]) {
            dp2.push_back(''R'');
            j--;
        }
    }
    cout << dp[n - 1][m - 1] << "\n";
    for (auto elem: dp2) {
        cout << elem << " ";
    }
    cout << "\n";
    return 0;
}'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n; cin >> n;
    vector<int> back_zero(n), back_one(n);
    back_zero[0] = 1;
    back_one[0] = 1;
    for (int i = 1; i < n; ++i) {
        back_zero[i] = (back_zero[i - 1] + back_one[i - 1]) % ((int)(pow(10, 9)) + 7);
        back_one[i] = back_zero[i - 1];
    }
    cout << (back_zero[n - 1] + back_one[n - 1]) % ((int)(pow(10, 9)) + 7) << "\n";
    return 0;
}
'),
(default, 'python', 'n = int(input())
back_a = [0 for i in range(n)]
back_other = [0 for j in range(n)]
back_a[0] = 1
back_other[0] = 2
for u in range(1, n):
    back_a[u] = (back_other[u - 1]) % (10 ** 9 + 7)
    back_other[u] = (2 * (back_a[u - 1] + back_other[u - 1])) % (10 ** 9 + 7)
print((back_a[-1] + back_other[-1]) % (10 ** 9 + 7))'),
(default, 'g++', '#include <bits/stdc++.h>
#define int long long
#define ld long double
#define p pair<int, int>
#define endl ''\n''
const int INF = (int)1e9+1;

using namespace std;

template <typename T>
void print(const T& a, bool len, bool plus, char sep = '' '') {
    if (len) {
        cout << a.size() << endl;
    }
    for (int q : a) {
        cout << q+plus << sep;
    }
    cout << endl;
}

signed main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    int n; cin >> n;
    vector<int> dp(max(3LL, n));
    dp[0] = 1, dp[1] = 2, dp[2] = 4;
    for (int i = 3; i < n; ++i) {
        dp[i] = dp[i - 1] + dp[i - 2] + dp[i  - 3];
    }
    cout << dp[n - 1];
    return 0;
}
'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n; cin >> n;
    vector<int> dp(max(3, n));
    dp[0] = 1, dp[1] = 2, dp[2] = 4;
    for (int i = 3; i < n; ++i) {
        dp[i] = dp[i - 1] + dp[i - 2] + dp[i  - 3];
    }
    cout << dp[n - 1];
    return 0;
}'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main() {
    int n; cin >> n;
    vector<int> not_dangerous(n), one_for_dangerous(n);
    not_dangerous[0] = 2;
    one_for_dangerous[0] = 1;
    for (int i = 1; i < n; ++i) {
        not_dangerous[i] = (one_for_dangerous[i - 1] + not_dangerous[i - 1]) * 2;
        one_for_dangerous[i] = not_dangerous[i - 1];
    }
    cout << not_dangerous[n - 1] + one_for_dangerous[n - 1] << "\n";
    return 0;
}'),
(default, 'python', 's = input()
left = 0
right = 1
for i in range(len(s)):
    if s[i] == "L":
        left = min(left, right) + 1
    elif s[i] == "R":
        right = min(left, right) + 1
    else:
        left += 1 
        right += 1 
print(right)
'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, m; cin >> n >> m;
    vector<vector<int>> a(n, vector<int>(m)), dp(n, vector<int>(m));
    vector<char> dp2;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> a[i][j];
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (i == 0 && j == 0) {
                dp[i][j] = a[i][j];
            }
            else if (i != 0 && j == 0) {
                dp[i][j] = dp[i - 1][j] + a[i][j];
            }
            else if (i == 0) {
                dp[i][j] = dp[i][j - 1] + a[i][j];
            }
            else {
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + a[i][j];
            }
        }
    }
    int i = n - 1, j = m - 1;
    while (i != 0 || j != 0) {
        if (i == 0) {
            dp2.push_back(''R'');
            j--;
        }
        else if (j == 0) {
            dp2.push_back(''D'');
            i--;
        }
        else if (dp[i - 1][j] >= dp[i][j - 1]) {
            dp2.push_back(''D'');
            i--;
        }
        else if (dp[i - 1][j] < dp[i][j - 1]) {
            dp2.push_back(''R'');
            j--;
        }
    }
    cout << dp[n - 1][m - 1] << "\n";
    for (auto elem: dp2) {
        cout << elem << " ";
    }
    cout << "\n";
    return 0;
}'),
(default, 'g++', '#include <bits/stdc++.h>
#define int long long

using namespace std;

signed main() {
    int n; cin >> n;
    vector<int> a(n), b(n), c(n), dp(n);
    for (int u = 0; u < n; ++u) {
        cin >> a[u] >> b[u] >> c[u];
    }
    dp[0] = a[0];
    if (n > 1) {
        dp[1] = min(b[0], a[0] + a[1]);
    }
    if (n > 2) {
        dp[2] = min(min(a[0] + a[1] + a[2], min(b[0] + a[2], a[0] + b[1])), c[0]);
    }
    for (int u = 3; u < n; ++u) {
        dp[u] = min(dp[u - 1] + a[u], min(dp[u - 2] + b[u - 1], dp[u - 3] + c[u - 2]));
    }
    cout << dp[n - 1] << "\n";
    return 0;
}'),
(default, 'python', 'x = int(input())
dp = [0 for i in range(x)]
ans = ''''
dp[0] = (0, '''')
dp[1] = (1, ''1'')
dp[2] = (1, ''3'')
for j in range(3, x):
    if (j + 1) % 3 != 0 and (j + 1) % 2 != 0:
        dp[j] = (dp[j - 1][0] + 1, dp[j - 1][1] + ''1'')
    elif (j + 1) % 3 != 0 and (j + 1) % 2 == 0:
        a, b = dp[(j + 1) // 2 - 1], dp[j - 1]
        if min(a[0], b[0]) == a[0]:
            dp[j] = (a[0] + 1, a[1] + ''2'')
        else:
            dp[j] = (b[0] + 1, b[1] + ''1'')
    elif (j + 1) % 3 == 0 and (j + 1) % 2 != 0:
        a, b = dp[(j + 1) // 3 - 1], dp[j - 1]
        if min(a[0], b[0]) == a[0]:
            dp[j] = (a[0] + 1, a[1] + ''3'')
        else:
            dp[j] = (b[0] + 1, b[1] + ''1'')
    else:
        a, b, c = dp[(j + 1) // 3 - 1], dp[(j + 1) // 2 - 1], dp[j - 1]
        if min(a[0], b[0], c[0]) == a[0]:
            dp[j] = (a[0] + 1, a[1] + ''3'')
        elif min(a[0], b[0], c[0]) == b[0]:
            dp[j] = (b[0] + 1, b[1] + ''2'')
        else:
            dp[j] = (c[0] + 1, c[1] + ''1'')
print(dp[-1][1])'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n; cin >> n;
    vector<int> back_zero(n), back_one(n);
    back_zero[0] = 1;
    back_one[0] = 1;
    for (int i = 1; i < n; ++i) {
        back_zero[i] = back_zero[i - 1] + back_one[i - 1];
        back_one[i] = back_zero[i - 1];
    }
    cout << back_zero[n - 1] + back_one[n - 1] << "\n";
    return 0;
}
'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main() {
    int n; cin >> n;
    vector<int> back_zero(n), back_one(n), back2_one(n);
    back_zero[0] = 1;
    back_one[0] = 1;
    back2_one[0] = 0;
    for (int i = 1; i < n; ++i) {
        back_one[i] = back_zero[i - 1];
        back2_one[i] = back_one[i - 1];
        back_zero[i] = back_one[i - 1] + back2_one[i - 1] + back_zero[i - 1];
    }
    cout << back_zero[n - 1] + back_one[n - 1] + back2_one[n - 1] << "\n";
    return 0;
}
'),
(default, 'g++', '#include <bits/stdc++.h>
#define int long long
#define ld long double
#define p pair<int, int>
#define endl ''\n''
const int INF = (int)1e9+1;

using namespace std;

template <typename T>
void print(const T& a, bool len, bool plus, char sep = '' '') {
    if (len) {
        cout << a.size() << endl;
    }
    for (int q : a) {
        cout << q+plus << sep;
    }
    cout << endl;
}

signed main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    cout << 1 << endl;
    return 0;
}
'),
(default, 'g++', '#include <bits/stdc++.h>
#define int long long

using namespace std;

int32_t main() {
    int n; cin >> n;
    vector<int> a(n), dp(n);
    dp[0] = 1;
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
    }
    for (int p = 1; p < n; ++p) {
        int t = 0;
        for (int j = 0; j < p; ++j) {
            if (a[p] > a[j]) {
                t = max(t, dp[j] + 1);
            }
        }
        dp[p] = t;
    }
    cout << dp[n - 1] << "\n";
    return 0;
}'),
(default, 'g++', '#include <bits/stdc++.h>
#define int long long

using namespace std;

signed main() {
    int n; cin >> n;
    vector<int> a(n), b(n), c(n), dp(n);
    for (int u = 0; u < n; ++u) {
        cin >> a[u] >> b[u] >> c[u];
    }
    dp[0] = 1;
    if (n > 1) {
        dp[1] = min(b[0], a[0] + a[1]);
    }
    if (n > 2) {
        dp[2] = min(min(a[0] + a[1] + a[2], min(b[0] + a[2], a[0] + b[1])), c[0]);
    }
    for (int u = 3; u < n; ++u) {
        dp[u] = min(min(dp[u - 1] + a[u], dp[u - 2] + min(a[u - 1] + a[u], b[u - 1])), dp[u - 3] + min(a[u] + a[u - 1] + a[u - 2], min(b[u - 2] + a[u], min(c[u - 2], b[u - 1] + a[u - 2]))));
    }
    cout << dp[n - 1] << "\n";
    return 0;
}'),
(default, 'python', 'n = int(input())
dp = [2]
for i in range(1, n):
    dp.append((dp[-1] - 1) * 2 + 1)
print(dp[-1])
'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, m; cin >> n >> m;
    vector<vector<int>> a(n, vector<int>(m)), dp(n, vector<int>(m));
    vector<char> dp2;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> a[i][j];
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (i == 0 && j == 0) {
                dp[i][j] = a[i][j];
            }
            else if (i != 0 && j == 0) {
                dp[i][j] = dp[i - 1][j] + a[i][j];
            }
            else if (i == 0) {
                dp[i][j] = dp[i][j - 1] + a[i][j];
            }
            else {
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + a[i][j];
            }
        }
    }
    int i = n - 1, j = m - 1;
    while (i != 0 || j != 0) {
        if (i == 0) {
            dp2.push_back(''R'');
            j--;
        }
        else if (j == 0) {
            dp2.push_back(''D'');
            i--;
        }
        else if (dp[i - 1][j] >= dp[i][j - 1]) {
            dp2.push_back(''D'');
            i--;
        }
        else if (dp[i - 1][j] < dp[i][j - 1]) {
            dp2.push_back(''R'');
            j--;
        }
    }
    cout << dp[n - 1][m - 1] << "\n";
    for (auto elem: dp2) {
        cout << elem << " ";
    }
    cout << "\n";
    return 0;
}

'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, m; cin >> n >> m;
    vector<vector<int>> a(n, vector<int>(m)), dp(n, vector<int>(m));
    vector<char> dp2;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> a[i][j];
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (i == 0 && j == 0) {
                dp[i][j] = a[i][j];
            }
            else if (i != 0 && j == 0) {
                dp[i][j] = dp[i - 1][j] + a[i][j];
            }
            else if (i == 0) {
                dp[i][j] = dp[i][j - 1] + a[i][j];
            }
            else {
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + a[i][j];
            }
        }
    }
    int i = n - 1, j = m - 1, iter = 0;
    while (i > 0 || j > 0) {
        if (i == 1 && j == 0) {
            dp2.push_back(''D'');
            j--;
            break;
        }
        else if (i == 0 && j == 1) {
            dp2.push_back(''R'');
            i--;
            break;
        }
        else if (j == 0) {
            dp2.push_back(''D'');
            i--;
        }
        else if (i == 0) {
            dp2.push_back(''R'');
            j--;
        }
        else if (dp[i - 1][j] >= dp[i][j - 1]) {
            dp2.push_back(''D'');
            i--;
        }
        else {
            dp2.push_back(''R'');
            j--;
        }
    }
    cout << dp[n - 1][m - 1] << "\n";
    for (int i = (int)dp2.size() - 1; i > -1; --i) {
        cout << dp2[i] << " ";
    }
    cout << "\n";
    return 0;
}'),
(default, 'g++', '#include <bits/stdc++.h>
#define int long long

using namespace std;


int32_t main() {
    int n; cin >> n;
    vector<int> dp(n + 1), p(n + 1);
    vector<char> ans;
    dp[1] = 0;
    p[1] = 1;
    if (n > 1) {
        dp[2] = 1;
        p[2] = 1;
    }
    if (n > 2) {
        dp[3] = 1;
        p[3] = 1;
    }
    for (int i = 4; i < n + 1; ++i) {
        dp[i] = dp[i - 1] + 1;
        p[i] = i - 1;
        if (i % 2 == 0 && dp[i / 2] + 1 <= dp[i]) {
            dp[i] = dp[i / 2] + 1;
            p[i] = i / 2;
        }
        if (i % 3 == 0 && dp[i / 3] + 1 <= dp[i]) {
            dp[i] = dp[i / 3] + 1;
            p[i] = i / 3;
        }
    }
    int i = n;
    while (p[i] != i) {
        if (p[i] == i - 1) {
            ans.push_back(''1'');
        }
        else if (i % 2 == 0 && p[i] == i / 2) {
            ans.push_back(''2'');
        }
        else if (i % 3 == 0 && p[i] == i / 3) {
            ans.push_back(''3'');
        }
        i = p[i];
    }
    for (int u = ans.size() - 1; u > -1; --u) {
        cout << ans[u];
    }
    cout << "\n";
    return 0;
}'),
(default, 'python', 's = input()
l = 0
r = 1
for i in range(len(s)):
    if s[i] == "L":
        l = min(l, r) + 1
    elif s[i] == "R":
        r = min(l, r) + 1
    else:
        l += 1 
        r += 1 
print(r)
'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, m; cin >> n >> m;
    vector<vector<int>> a(n, vector<int>(m)), dp(n, vector<int>(m));
    vector<char> dp2;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> a[i][j];
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (i == 0 && j == 0) {
                dp[i][j] = a[i][j];
            }
            else if (i != 0 && j == 0) {
                dp[i][j] = dp[i - 1][j] + a[i][j];
            }
            else if (i == 0) {
                dp[i][j] = dp[i][j - 1] + a[i][j];
            }
            else {
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + a[i][j];
            }
        }
    }
    int i = n - 1, j = m - 1;
    while (i != 0 || j != 0) {
        if (i == 0) {
            dp2.push_back(''R'');
            j--;
        }
        else if (j == 0) {
            dp2.push_back(''D'');
            i--;
        }
        else if (dp[i - 1][j] >= dp[i][j - 1]) {
            dp2.push_back(''D'');
            i--;
        }
        else if (dp[i - 1][j] < dp[i][j - 1]) {
            dp2.push_back(''R'');
            j--;
        }
    }
    cout << dp[n - 1][m - 1] << "\n";
    for (auto elem: dp2) {
        cout << elem << " ";
    }
    cout << "\n";
    return 1;
}'),
(default, 'g++', '#include <bits/stdc++.h>
#define int long long
#define ld long double
#define p pair<int, int>
#define endl ''\n''
const int INF = (int)1e9+1;

using namespace std;

template <typename T>
void print(const T& a, bool len, bool plus, char sep = '' '') {
    if (len) {
        cout << a.size() << endl;
    }
    for (int q : a) {
        cout << q+plus << sep;
    }
    cout << endl;
}

signed main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    cout << 1 << endl;
    return 0;
}
'),
(default, 'python', 'print(1)'),
(default, 'g++', '#include <bits/stdc++.h>
#define int long long
#define ld long double
#define p pair<int, int>
#define endl ''\n''
const int INF = (int)1e9+1;

using namespace std;

template <typename T>
void print(const T& a, bool len, bool plus, char sep = '' '') {
    if (len) {
        cout << a.size() << endl;
    }
    for (int q : a) {
        cout << q+plus << sep;
    }
    cout << endl;
}

signed main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    cout << 1 << endl;
    return 0;
}'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main() {
    int n; cin >> n;
    vector<int> a(n), dp(n + 1);
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
    }
    dp[0] = 0;
    dp[1] = a[0];
    dp[2] = a[1];
    for (int j = 3; j <= n; ++j) {
        dp[j] = min(dp[j - 1] + a[j - 1], dp[j - 2] + a[j - 1]);
    }
    cout << dp[n] << "\n";
    return 0;
}'),
(default, 'python', 'x = int(input())
dp = [0 for i in range(x)]
ans = ''''
dp[0] = (0, '''')
if x > 1:
    dp[1] = (1, ''1'')
if x > 2:
    dp[2] = (1, ''3'')
for j in range(3, x):
    if (j + 1) % 3 != 0 and (j + 1) % 2 != 0:
        dp[j] = (dp[j - 1][0] + 1, dp[j - 1][1] + ''1'')
    elif (j + 1) % 3 != 0 and (j + 1) % 2 == 0:
        a, b = dp[(j + 1) // 2 - 1], dp[j - 1]
        if min(a[0], b[0]) == a[0]:
            dp[j] = (a[0] + 1, a[1] + ''2'')
        else:
            dp[j] = (b[0] + 1, b[1] + ''1'')
    elif (j + 1) % 3 == 0 and (j + 1) % 2 != 0:
        a, b = dp[(j + 1) // 3 - 1], dp[j - 1]
        if min(a[0], b[0]) == a[0]:
            dp[j] = (a[0] + 1, a[1] + ''3'')
        else:
            dp[j] = (b[0] + 1, b[1] + ''1'')
    else:
        a, b, c = dp[(j + 1) // 3 - 1], dp[(j + 1) // 2 - 1], dp[j - 1]
        if min(a[0], b[0], c[0]) == a[0]:
            dp[j] = (a[0] + 1, a[1] + ''3'')
        elif min(a[0], b[0], c[0]) == b[0]:
            dp[j] = (b[0] + 1, b[1] + ''2'')
        else:
            dp[j] = (c[0] + 1, c[1] + ''1'')
print(dp[-1][1])'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, m; cin >> n >> m;
    vector<vector<int>> a(n, vector<int>(m)), dp(n, vector<int>(m));
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> a[i][j];
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (i == 0 && j == 0) {
                dp[i][j] = a[i][j];
            }
            else if (i != 0 && j == 0) {
                dp[i][j] = dp[i - 1][j] + a[i][j];
            }
            else if (i == 0) {
                dp[i][j] = dp[i][j - 1] + a[i][j];
            }
            else {
                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + a[i][j];
            }
        }
    }
    cout << dp[n - 1][m - 1] << "\n";
    return 0;
}'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n; cin >> n;
    vector<int> back_zero(n), back_one(n);
    back_zero[0] = 1;
    back_one[0] = 1;
    for (int i = 1; i < n; ++i) {
        back_zero[i] = back_zero[i - 1] * 2;
        back_one[i] = back_one[i - 1];
    }
    cout << back_zero[n - 1] + back_one[n - 1] << "\n";
    return 0;
}
'),
(default, 'g++', '#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, m; cin >> n >> m;
    vector<vector<int>> a(n, vector<int>(m)), dp(n, vector<int>(m));
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> a[i][j];
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (i == 0 && j == 0) {
                dp[i][j] = 1;
            }
            else if (i != 0 && j == 0 && a[i][j] == 1) {
                dp[i][j] = dp[i - 1][j];
            }
            else if (i == 0 && a[i][j] == 1) {
                dp[i][j] = dp[i][j - 1];
            }
            else if (a[i][j] != 0) {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
    }
    if (dp[n - 1][m - 1] == 0) {
        cout << "Impossible" << "\n";
    }
    else {
        cout << dp[n - 1][m - 1] << "\n";
    }
    return 0;
}'),
(default, 'python', 'n = int(input())
dp = [1, 1, 2] + [0] * (n - 1)
for i in range(3, n + 1):
    dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3]
print(dp[n])'),
(default, 'python', 'st = input()
s = ""
ans = 0
for i in st:
    if i != ''B'':
        s += i
    else:
        ans += 1
n = len(s)
dp = [0] * (n + 1)
side = ''L''
for i in range(1, n + 1):
    if s[i - 1] == side:
        dp[i] = dp[i - 1] + 1
        side = chr(ord(''L'') + ord(''R'') - ord(s[i - 1]))
    else:
        dp[i] = dp[i - 1]
if side == ''L'':
    print(dp[-1] + ans + 1)
else:
    print(dp[-1] + ans)
'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    cin >> n;
    vector<vector<int>> queue(n + 1, vector<int>(3));
    for (int i = 1; i <= n; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        queue[i] = {a, b, c};
    }
    vector<int> people(n + 1, 0);
    int i = 1;
    while (i <= n) {
        people[i] = people[i - 1];
        if (i + 2 <= n && queue[i][2] < queue[i][0] + min(queue[i + 1][1], queue[i + 1][0] + queue[i + 2][0])) {
            people[i] += queue[i][2];
            people[i + 1] = people[i];
            people[i + 2] = people[i];
            i += 3;
        } else if (i + 1 <= n && queue[i][1] < queue[i][0] + queue[i + 1][0]) {
            people[i] += queue[i][1];
            people[i + 1] = people[i];
            i += 2;
        } else {
            people[i] += queue[i][0];
            i += 1;
        }
    }
    cout << people[n];
    return 0;
}'),
(default, 'python', 'n = int(input())
if n == 1:
    print(2)
elif n == 2:
    print(4)
else:
    dp = [0] * n
    dp[0] = 2
    dp[1] = 4
    dp[2] = 7
    for i in range(3, n):
        dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3]
    print(dp[-1])
'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    cin >> n;
    vector<vector<int>> queue(n + 1, vector<int>(3));
    for (int i = 1; i <= n; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        queue[i] = {a, b, c};
    }
    vector<long long> people(n + 1, 0);
    int i = 1;
    while (i <= n) {
        people[i] = people[i - 1];
        if (i + 2 <= n && queue[i][2] < min(queue[i][1] + queue[i + 2][0], queue[i][0] +
        min(queue[i + 1][1], queue[i + 1][0] + queue[i + 2][0]))) {
            people[i] += queue[i][2];
            people[i + 1] = people[i];
            people[i + 2] = people[i];
            i += 3;
        } else if (i + 1 <= n && queue[i][1] < queue[i][0] + queue[i + 1][0]) {
            people[i] += queue[i][1];
            people[i + 1] = people[i];
            i += 2;
        } else {
            people[i] += queue[i][0];
            i += 1;
        }
    }
    cout << people[n];
    return 0;
}'),
(default, 'python', 'n = int(input())
if n == 1:
    print(3)
elif n == 2:
    print(8)
else:
    dp = [0] * n
    dp[0] = 3
    dp[1] = 8
    for i in range(2, n):
        dp[i] = (dp[i - 1] + dp[i - 2]) * 2
    print(dp[-1] % 1000000007)
'),
(default, 'python', 'n = int(input())
if n == 1:
    print(2)
else:
    dp = [0] * n
    dp[0] = 2
    dp[1] = 3
    for i in range(2, n):
        dp[i] = dp[i - 1] + dp[i - 2]
    print(dp[-1])
'),
(default, 'python', 'n = int(input())
a = list(map(int, input().split()))
dp = [1] * n
for u in range(1, n):
    for y in range(u):
        if a[u] > a[y]:
            dp[u] = max(dp[u], dp[y] + 1)
ans = 0
for elem in dp:
    ans = max(ans, elem)
print(ans)'),
(default, 'python', 'n = int(input())
if n == 0 or n == 1:
    print(1)
elif n == 2:
    print(2)
else:
    dp = [0] * (n + 1)
    dp[0] = 1
    dp[1] = 1
    dp[2] = 2
    for i in range(3, n + 1):
        dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3]
    print(dp[-1])
'),
(default, 'python', 'n = int(input())
arr = [int(i) for i in input().split()]
if n == 1:
    print(arr[0])
else:
    dp = [0] * n
    dp[0] = arr[0]
    dp[1] = arr[1]
    for i in range(2, n):
        dp[i] = min(dp[i - 1], dp[i - 2]) + arr[i]
    print(dp[-1])
'),
(default, 'python', 'n = int(input())
if n == 1:
    print(3)
elif n == 2:
    print(8)
else:
    dp = [0] * n
    dp[0] = 3
    dp[1] = 8
    for i in range(2, n):
        dp[i] = (dp[i - 1] + dp[i - 2]) * 2
    print(dp[-1])
'),
(default, 'python', 'n = int(input())
arr = [int(i) for i in input().split()]
arr.sort()
dp1 = [0] * n
dp2 = [0] * n
dp1[0] = 1000000000
for i in range(1, n):
    dp1[i] = min(dp1[i - 1], dp2[i - 1]) + arr[i] - arr[i - 1]
    dp2[i] = dp1[i - 1]
print(dp1[-1])
'),
(default, 'python', 'n = int(input())
if n == 1:
    print(3)
elif n == 2:
    print(8)
else:
    dp = [0] * n
    dp[0] = 3
    dp[1] = 8
    dp[2] = 24
    for i in range(4, n):
        dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3]
    print(dp[-1])
'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;

int main() {
    string s;
    cin >> s;
    int n = s.size();
    vector<vector<int>> river(n + 1, vector<int>(2));
    river[0][0] = 0;
    river[0][1] = 1;
    for (int i = 1; i <= n; i++) {
        if (s[i - 1] == ''L'') {
            river[i][0] = river[i - 1][0] + 1;
            river[i][1] = min(river[i][0] + 1, river[i - 1][1]);
            river[i][0] = min(river[i][0], river[i][1] + 1);
        } else if (s[i - 1] == ''R'') {
            river[i][1] = river[i - 1][1] + 1;
            river[i][0] = min(river[i][1] + 1, river[i - 1][0]);
            river[i][1] = min(river[i][1], river[i][0] + 1);
        } else if (s[i - 1] == ''B'') {
            river[i][0] = river[i - 1][0] + 1;
            river[i][1] = river[i - 1][1] + 1;
        }
    }
    cout << min(river[n][0] + 1, river[n][1]);
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    cin >> n;
    vector<vector<int>> queue(n + 1, vector<int>(3));
    for (int i = 1; i <= n; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        queue[i] = {a, b, c};
    }
    vector<long long> people(n + 1, 0);
    people[1] = queue[1][0];
    if (n == 1) {
        cout << people[1];
        return 0;
    }
    people[2] = min(queue[1][0] + queue[2][1], queue[1][1]);
    for (int i = 2; i <= n; i++) {
        people[i] = min(people[i - 1] + queue[i][0], min(people[i - 2] + queue[i - 1][1], people[i - 3] +
        queue[i - 2][2]));
    }
    cout << people[n];
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> stairs(n + 1);
    stairs[0] = 1;
    stairs[1] = 1;
    if (n == 1) {
        cout << stairs[1];
        return 0;
    }
    stairs[2] = stairs[0] + stairs[1];
    for (int i = 3; i <= n; i++) {
        stairs[i] = stairs[i - 3] + stairs[i - 2] + stairs[i - 1];
    }
    cout << stairs[n];
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    cin >> n;
    if (n == 1) {
        cout << 2;
        return 0;
    }
    vector<vector<int>> seq(n + 1, vector<int>(4));
    seq[1][0] = 1;
    seq[1][1] = 1;
    seq[1][2] = 1;
    seq[1][3] = 1;
    for (int i = 2; i < n; i++) {
        seq[i][0] = seq[i - 1][2] + seq[i - 1][0];
        seq[i][1] = seq[i - 1][2] + seq[i - 1][0];
        seq[i][2] = seq[i - 1][1] + seq[i - 1][3];
        seq[i][3] = seq[i - 1][1];
    }
    cout << seq[n - 1][0] + seq[n - 1][1] + seq[n - 1][2] + seq[n - 1][3];
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> tablet(n);
    for (int i = 0; i < n; i++) {
        cin >> tablet[i];
    }
    int ans = 0;
    vector<pair<int, int>> threads;
    ans += tablet[1] - tablet[0];
    threads.push_back(make_pair(0, 1));
    for (int i = 1; i < n - 1; i++) {
        if (tablet[i + 1] - tablet[i] <= tablet[i] - tablet[i - 1]) {
            ans += tablet[i + 1] - tablet[i];
            threads.push_back(make_pair(i, i + 1));
        } else {
            bool flag = true;
            for (auto x : threads) {
                if (x.first == i - 1 && x.second == i) {
                    flag = false;
                }
            }
            if (flag) {
                ans += tablet[i] - tablet[i - 1];
                threads.push_back(make_pair(i - 1, i));
            }
        }
    }
    cout << ans;
    return 0;
}'),
(default, 'python', 'n = int(input())
a = list(map(int, input().split()))
m = int(input())
b = list(map(int, input().split()))
dp = [[0] * (m + 1) for j in range(n + 1)]
ans_dp = [[[]] * (m + 1) for t in range(n + 1)]
for u in range(1, n + 1):
    for y in range(1, m + 1):
        if a[u - 1] == b[y - 1]:
            dp[u][y] = dp[u - 1][y - 1] + 1
            ans_dp[u][y] = ans_dp[u - 1][y - 1] + [a[u - 1]]
        else:
            dp[u][y] = max(dp[u - 1][y], dp[u][y - 1])
            if dp[u][y] == dp[u - 1][y]:
                ans_dp[u][y] = ans_dp[u - 1][y]
            else:
                ans_dp[u][y] = ans_dp[u][y - 1]
print(dp[n][m])
print(*ans_dp[n][m])'),
(default, 'g++', '#include <iostream>
#include <vector>
#define endl "\n"
#define int long long

using namespace std;

void solve() {
    int n; cin >> n;
    vector<int> a(n), dp(n);
    bool last = false;
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
    }
    for (int p = 1; p < n; ++p) {
        if (p == n - 1 && !last) {
            dp[p] = dp[p - 1] + a[p] - a[p - 1];
        }
        else if (p == 1) {
            dp[p] = a[p] - a[p - 1];
        }
        else if (!last) {
            dp[p] = min(a[p] - a[p - 1], a[p + 1] - a[p]) + dp[p - 1];
            if (min(a[p] - a[p - 1], a[p + 1] - a[p]) == a[p + 1] - a[p]) {
                last = true;
            }
        }
        else {
            last = false;
            dp[p] = dp[p - 1];
        }
    }
    cout << dp[n - 1] << endl;
}


signed main() {
    solve();
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>
#include <string>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<string> ans(n + 1);
    ans[1] = "";
    for (int i = 2; i <= n; i++) {
        int cnt = i - 1;
        char num = ''1'';
        if (i % 3 == 0) {
            if (ans[i / 3].size() < ans[cnt].size()) {
                cnt = i / 3;
                num = ''3'';
            }
        }
        if (i % 2 == 0) {
            if (ans[i / 2].size() < ans[cnt].size()) {
                cnt = i / 2;
                num = ''2'';
            }
        }
        ans[i] = ans[cnt] + num;
    }
    cout << ans[n];
    return 0;
}'),
(default, 'python', 'n = int(input())
if n == 1:
    print(2)
else:
    len2 = 2
    len1 = 3
    for i in range(2, n):
        len0 = len1 + len2
        len1, len2 = len0, len1
    print(len1 % 1000000007)
'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> stairs(n);
    for (int i = 0; i < n; i++) {
        cin >> stairs[i];
    }
    vector<int> ans(n + 1, 0);
    ans[1] = stairs[0];
    for (int i = 2; i <= n; i++) {
        ans[i] = min(ans[i - 1], ans[i - 2]) + stairs[i - 1];
    }
    cout << ans[n];
    return 0;
}'),
(default, 'python', 'n = int(input())
arr = [int(i) for i in input().split()]
dp = [0] * n
dp[0] = arr[0]
dp[1] = arr[1]
for i in range(2, n):
    dp[i] = min(dp[i - 1], dp[i - 2]) + arr[i]
print(dp[-1])
'),
(default, 'python', 'n = int(input())
a = []
b = []
c = []
for i in range(n):
    a0, b0, c0 = map(int, input().split())
    a.append(a0)
    b.append(b0)
    c.append(c0)
dp = [0] * n
dp[0] = a[0]
if n >= 2:
    dp[1] = min(dp[0] + a[1], b[0])
if n >= 3:
    dp[2] = min(dp[1] + a[2], dp[0] + b[1], c[0])
for i in range(3, n):
    dp[i] = min(dp[i - 1] + a[i], dp[i - 2] + b[i - 1], dp[i - 3] + c[i - 2])
print(dp[-1])
'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;

int main() {
    const int MOD = 1e9 + 7;
    int n;
    cin >> n;
    vector<vector<int>> pile(n + 1, vector<int>(3));
    pile[1][0] = 1;
    pile[1][1] = 1;
    pile[1][2] = 1;
    for (int i = 2; i <= n; i++) {
        pile[i][0] = (pile[i - 1][1] + pile[i - 1][2]) % MOD;
        pile[i][1] = (pile[i - 1][0] + pile[i - 1][1] + pile[i - 1][2]) % MOD;
        pile[i][2] = (pile[i - 1][0] + pile[i - 1][1] + pile[i - 1][2]) % MOD;
    }
    cout << pile[n][0] + pile[n][1] + pile[n][2];
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;

int main() {
    const int MOD = 1e9 + 7;
    int n;
    cin >> n;
    vector<vector<long long>> pile(n + 1, vector<long long>(3));
    pile[1][0] = 1;
    pile[1][1] = 1;
    pile[1][2] = 1;
    for (int i = 2; i <= n; i++) {
        pile[i][0] = (pile[i - 1][1] + pile[i - 1][2]) % MOD;
        pile[i][1] = (pile[i - 1][0] + pile[i - 1][1] + pile[i - 1][2]) % MOD;
        pile[i][2] = (pile[i - 1][0] + pile[i - 1][1] + pile[i - 1][2]) % MOD;
    }
    cout << (pile[n][0] + pile[n][1] + pile[n][2]) % MOD;
    return 0;
}'),
(default, 'python', 'n = int(input())
dp = [0] * (n + 1)
dp[0] = 1
dp[1] = 1
dp[2] = 2
for i in range(3, n + 1):
    dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3]
print(dp[-1])
'),
(default, 'python', 'n = int(input())
if n == 1:
    print(2)
else:
    dp = [0] * n
    dp[0] = 2
    dp[1] = 3
    for i in range(2, n):
        dp[i] = dp[i - 1] + dp[i - 2]
    print(dp[-1] % 1000000007)
'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;

int main() {
    const int MOD = 1e9 + 7;
    long long n;
    cin >> n;
    vector<vector<long long>> seq(n + 1, vector<long long>(2));
    seq[1][0] = 1;
    seq[1][1] = 1;
    for (int i = 2; i <= n; i++) {
        seq[i][0] = (seq[i - 1][0] + seq[i - 1][1]) % MOD;
        seq[i][1] = seq[i - 1][0];
    }
    cout << (seq[n][0] + seq[n][1]) % MOD;
    return 0;
}'),
(default, 'python', 'n = int(input())
a = list(map(int, input().split()))
m = int(input())
b = list(map(int, input().split()))
dp = [[0] * (m + 1) for j in range(n + 1)]
for u in range(1, n + 1):
    for y in range(1, m + 1):
        if a[u - 1] == b[y - 1]:
            dp[u][y] = dp[u - 1][y - 1] + 1
        else:
            dp[u][y] = max(dp[u - 1][y], dp[u][y - 1])
print(dp[n][m])'),
(default, 'python', 'n = int(input())
if n == 1:
    print(3)
elif n == 2:
    print(8)
else:
    dp = [0] * n
    dp[0] = 3
    dp[1] = 8
    for i in range(2, n):
        dp[i] = ((dp[i - 1] + dp[i - 2]) * 2) % 1000000007
    print(dp[-1])
'),
(default, 'g++', '#include <iostream>
#include <vector>
#include <string>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> mass(n);
    for (int i = 0; i < n; i++) {
        cin >> mass[i];
    }
    vector<int> ans(n, 1);
    for (int i = 1; i < n; i++) {
        int cnt = -1;
        for (int j = 0; j < i; j++) {
            if (mass[j] < mass[i] && ans[j] > cnt) {
                cnt = ans[j];
            }
        }
        ans[i] = cnt + 1;
    }
    int res = 0;
    for (int i = 0; i < n; i++) {
        res = max(res, ans[i]);
    }
    cout << res;
    return 0;
}'),
(default, 'python', 'from sys import stdin

input1 = stdin.readline
N, M = map(int, input1().split())
numbers = [list(map(int, input().split())) for __ in range(N)]
for i in range(N - 1, -1, -1):
    for j in range(M - 1, -1, -1):
        if numbers[i][j] == 1:
            numbers[i][j] = 0
            if j < M - 1:
                numbers[i][j] += numbers[i][j + 1]
            if i < N - 1:
                numbers[i][j] += numbers[i + 1][j]
            numbers[i][j] = max(numbers[i][j], 1)
            
print(numbers[0][0])'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;


int main() {
    int n, m;
    cin >> n >> m;
    vector<vector<int>> mass(n, vector<int>(m, 0));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> mass[i][j];
        }
    }
    vector<vector<int>> ans(n + 2, vector<int>(m + 2, 0));
    ans[0][1] = 1;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (mass[i - 1][j - 1] != 0) {
                ans[i][j] = ans[i - 1][j] + ans[i][j - 1];
            }
        }
    }
    if (ans[n][m] == 0) {
        cout << "Impossible";
        return 0;
    }
    cout << ans[n][m];
    return 0;
}'),
(default, 'python', 'from sys import stdin

input1 = stdin.readline
N, M = map(int, input1().split())
numbers = [list(map(int, input().split())) for __ in range(N)]

dp = [[0] * M for _ in range(N)]
dp[0][0] = 1

for i in range(N):
    for j in range(M):
        
        if numbers[i][j] == 0:
            dp[i][j] = 0
        else:
            if i > 0:
                dp[i][j] += dp[i - 1][j]
            if j > 0:
                dp[i][j] += dp[i][j - 1]
if dp[N - 1][M - 1] == 0:
    print("Impossible")
else:
    print(dp[N - 1][M - 1])'),
(default, 'python', 'from sys import stdin

input1 = stdin.readline
n = int(input1())

dp = [[0, 0] for i in range(n)]

dp[0][0] = 2
dp[0][1] = 1

for i in range(1, n):
    dp[i][0] = ((dp[i - 1][1] + dp[i - 1][0]) * 2)
    dp[i][1] = dp[i - 1][0]

print((dp[n - 1][1] + dp[n - 1][0]) % (10 ** 9 + 7))'),
(default, 'python', 'from sys import stdin

input1 = stdin.readline
n = int(input1())

dp = [[0, 0] for i in range(n)]

dp[0][0] = 2
dp[0][1] = 1

for i in range(1, n):
    dp[i][0] = (dp[i - 1][1] + dp[i - 1][0]) * 2
    dp[i][1] = dp[i - 1][0]

print(dp[n - 1][1] + dp[n - 1][0])'),
(default, 'python', 'from sys import stdin


def step(N):
    m = [[0, 0, 0] for _ in range(max(2, N))]

    m[0][0] = 1
    m[0][1] = 1
    m[1][0] = 2
    m[1][1] = 1
    m[1][2] = 1
    i = 2

    while i < N:
        m[i][0] = m[i - 1][0] + m[i - 1][1] + m[i - 1][2]
        m[i][1] = m[i - 1][0]
        m[i][2] = m[i - 1][1]
        i += 1

    return m[i - 1][1] + m[i - 1][0] + m[i - 1][2]


input1 = stdin.readline
n = int(input1())
print(step(n))'),
(default, 'g++', '#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

#define int long long
 
signed main() {
    int n;
    cin >> n;
    vector<pair<int, int>> a(n + 1);
    a[0] = {1,1};
    for (int i = 1; i < n; i++) {
        a[i].first = a[i - 1].first + a[i - 1].second;
        a[i].second = a[i - 1].first;
    }
    cout << a[n - 1].first + a[n - 1].second;
}'),
(default, 'g++', '#include <iostream>
#include <vector>
#include <string>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> seq1(n);
    for (int i = 0; i < n; i++){
        cin >> seq1[i];
    }
    int m;
    cin >> m;
    vector<int> seq2(m);
    for (int i = 0; i < m; i++) {
        cin >> seq2[i];
    }
    vector<vector<int>> ans(n + 2, vector<int>(m + 2, 0));
    ans[1][1] = 1;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (i + j > 2) {
                if (seq1[i - 1] == seq2[j - 1]) {
                    ans[i][j] = ans[i - 1][j - 1] + 1;
                } else {
                    ans[i][j] = max(ans[i - 1][j], ans[i][j - 1]);
                }
            }
        }
    }
    cout << ans[n][m];
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> mass(n);
    for (int i = 0; i < n; i++) {
        cin >> mass[i];
    }
    vector<vector<int>> ans(n + 1, vector<int>(0));
    ans[0].push_back(mass[0]);
    for (int i = 1; i < n; i++) {
        int cnt = n;
        for (int j = 0; j < i; j++) {
            if (mass[j] < mass[i] && ans[j].size() > ans[cnt].size()) {
                cnt = j;
            }
        }
        ans[i] = ans[cnt];
        ans[i].push_back(mass[i]);
    }
    int max_cnt = 0;
    int res = 0;
    for (int i = 0; i < n; i++) {
        if (max_cnt < ans[i].size()) {
            max_cnt = ans[i].size();
            res = i;
        }
    }
    for (auto x : ans[res]) {
        cout << x << " ";
    }
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>
#include <string>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> mass(n);
    for (int i = 0; i < n; i++) {
        cin >> mass[i];
    }
    vector<int> ans(n, 1);
    for (int i = 1; i < n; i++) {
        int cnt = 0;
        for (int j = 0; j < i; j++) {
            if (mass[j] < mass[i] && ans[j] > cnt) {
                cnt = ans[j];
            }
        }
        ans[i] = cnt + 1;
    }
    int res = 0;
    for (int i = 0; i < n; i++) {
        res = max(res, ans[i]);
    }
    cout << res;
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

#define int long long
 
signed main() {
    int n;
    cin >> n;
    int mod = 1e9 + 7;
    vector<pair<int, int>> a(n + 1);
    a[0] = {1,1};
    for (int i = 1; i < n; i++) {
        a[i].first  = (a[i - 1].first + a[i - 1].second) %  mod;
        a[i].second = a[i - 1].first;
    }
    cout << (a[n - 1].first + a[n - 1].second) % mod;
}
'),
(default, 'python', 'from sys import stdin


def step(N, M):
    M[N - 2] += M[N - 1]
    for i in range(N - 3, -1, -1):
        M[i] += min(M[i + 1], M[i + 2])
    return M[0]


input1 = stdin.readline
n = int(input1())
m1 = list(map(int, input1().split()))
print(step(n, m1))'),
(default, 'python', 'from sys import stdin

input1 = stdin.readline
n = int(input1())

dp = [[0, 0] for i in range(n)]

dp[0][0] = 2
dp[0][1] = 1
const = 1000000000 + 7
for i in range(1, n):
    dp[i][0] = (dp[i - 1][1] + dp[i - 1][0]) * 2 % const
    dp[i][1] = dp[i - 1][0]

print(dp[n - 1][1] + dp[n - 1][0])'),
(default, 'g++', '#include <iostream>
#include <vector>
#include <string>

using namespace std;


int main() {
    const int INF = 1e9 + 179;
    int n, m;
    cin >> n >> m;
    vector<vector<int>> mass(n, vector<int>(m, 0));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> mass[i][j];
        }
    }
    vector<vector<int>> cnt(n + 2, vector<int>(m + 2, -INF));
    vector<vector<string>> ans(n, vector<string>(m, ""));
    cnt[0][1] = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (cnt[i - 1][j] >= cnt[i][j - 1]) {
                cnt[i][j] = cnt[i - 1][j] + mass[i - 1][j - 1];
                if (i + j > 2) {
                    ans[i - 1][j - 1] = ans[i - 2][j - 1] + "D ";
                }
            } else {
                cnt[i][j] = cnt[i][j - 1] + mass[i - 1][j - 1];
                ans[i - 1][j - 1] = ans[i - 1][j - 2] + "R ";
            }
        }
    }
    cout << cnt[n][m] << endl;
    cout << ans[n - 1][m - 1];
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> mass(n);
    for (int i = 0; i < n; i++) {
        cin >> mass[i];
    }
    vector<vector<int>> ans(n + 1, vector<int>(0));
    ans[0].push_back(mass[0]);
    for (int i = 1; i < n; i++) {
        int cnt = n;
        for (int j = 0; j < i; j++) {
            if (mass[j] < mass[i] && ans[j].size() > ans[cnt].size()) {
                cnt = j;
            }
        }
        ans[i] = ans[cnt];
        ans[i].push_back(mass[i]);
    }
    int max_cnt = 0;
    int res = 0;
    for (int i = 0; i < n; i++) {
        if (max_cnt < ans[i].size()) {
            max_cnt = ans[i].size();
            res = i;
        }
    }
    for (auto x : ans[res]) {
        cout << x << " ";
    }
    return 0;
}'),
(default, 'python', 'from sys import stdin


def step(N):
    m = [[None, None] for _ in range(N)]

    m[0][0] = 1
    m[0][1] = 1
    i = 1
    while i < N:
        m[i][0] = m[i - 1][0] + m[i - 1][1]
        m[i][1] = m[i - 1][0]
        i += 1

    return (m[i - 1][1] + m[i - 1][0]) % (10 ** 9 + 7)


input1 = stdin.readline
n = int(input1())
print(step(n))'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;


int main() {
    const int INF = 1e9 + 179;
    int n, m;
    cin >> n >> m;
    vector<vector<int>> mass(n, vector<int>(m, 0));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> mass[i][j];
        }
    }
    vector<vector<int>> ans(n + 2, vector<int>(m + 2, INF));
    ans[0][1] = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            ans[i][j] = min(ans[i - 1][j], ans[i][j - 1]) + mass[i - 1][j - 1];
        }
    }
    cout << ans[n][m];
    return 0;
}'),
(default, 'python', 'from sys import stdin


def step(N):
    m = [None] * max(3, N + 1)
    m[0] = 1
    m[1] = 1
    m[2] = 2
    i = 3
    while i < N + 1:
        m[i] = m[i - 1] + m[i - 2] + m[i - 3]
        i += 1
    return m[N]


input1 = stdin.readline
n = int(input1())
print(step(n))'),
(default, 'python', 'from sys import stdin

input1 = stdin.readline
n = int(input1())

dp = [[0, 0] for i in range(3)]

dp[0][0] = 2
dp[0][1] = 1

for i in range(1, n):
    dp[i % 2][0] = ((dp[(i + 1) % 2][1] + dp[(i + 1) % 2][0]) * 2)
    dp[i % 2][1] = dp[(i + 1) % 2][0]

print((dp[(n - 1) % 2][1] + dp[(n - 1) % 2][0]) % (10 ** 9 + 7))'),
(default, 'python', 'from sys import stdin


def step(N):
    m = [[None, None] for _ in range(N)]

    m[0][0] = 1
    m[0][1] = 1
    i = 1
    while i < N:
        m[i][0] = m[i - 1][0] + m[i - 1][1]
        m[i][1] = m[i - 1][0]
        i += 1
    
    return m[i - 1][1] + m[i - 1][0]


input1 = stdin.readline
n = int(input1())
print(step(n))'),
(default, 'python', 'from sys import stdin


def step(N):
    m = [[0, 0, 0] for _ in range(max(2, N))]

    m[0][0] = 1
    m[0][1] = 1
    m[1][0] = 2
    m[1][1] = 1
    m[1][2] = 1
    i = 2

    while i < N:
        m[i][0] = m[i - 1][0] + m[i - 1][1] + m[i - 1][2]
        m[i][1] = m[i - 1][0]
        m[i][2] = m[i - 1][1]
        i += 1

    return m[N - 1][1] + m[N - 1][0] + m[N - 1][2]


input1 = stdin.readline
n = int(input1())
print(step(n))'),
(default, 'python', 'from sys import stdin


def step(N, M):
    M[N - 2] += M[N - 1]
    for i in range(N - 3, -1, -1):
        M[i] += min(M[i + 1], M[i + 2])
    
    return M[0]


input1 = stdin.readline
n = 1 + int(input1())
m1 = [0] + list(map(int, input1().split()))
print(step(n, m1))'),
(default, 'python', 'from sys import stdin


def step(N):
    m = [[None, None] for _ in range(N)]

    m[0][0] = 1
    m[0][1] = 1
    i = 1
    cons = 10 ** 9 + 7
    while i < N:
        m[i][0] = (m[i - 1][0] + m[i - 1][1]) % cons

        m[i][1] = m[i - 1][0] % cons
        i += 1

    return (m[i - 1][1] + m[i - 1][0]) % cons


input1 = stdin.readline
n = int(input1())
print(step(n))'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> mass(n);
    for (int i = 0; i < n; i++) {
        cin >> mass[i];
    }
    vector<vector<int>> ans(n + 1, vector<int>(0));
    ans[0].push_back(mass[0]);
    for (int i = 1; i < n; i++) {
        int cnt = n;
        for (int j = 0; j < i; j++) {
            if (mass[j] < mass[i] && ans[j].size() > ans[cnt].size()) {
                cnt = j;
            }
        }
        ans[i] = ans[cnt];
        ans[i].push_back(mass[i]);
    }
    int max_cnt = 0;
    int res = 0;
    for (int i = 0; i < n; i++) {
        if (max_cnt < ans[i].size()) {
            max_cnt = ans[i].size();
            res = i;
        }
    }
    for (auto x : ans[res]) {
        cout << x << " ";
    }
    return 0;

}'),
(default, 'g++', '#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

#define int long long
 
signed main() {
    int n;
    cin >> n;
    vector<int> a(n + 1);
    a[0] = 1;
    for (int i = 1; i < n + 1; i++) {
        if (i - 1 >= 0){
            a[i] += a[i - 1];
        }
        if (i - 2 >= 0){
            a[i] += a[i - 2];
        }
        if (i - 3 >= 0){
            a[i] += a[i - 3];
        }
    }
    cout << a[n];
}
'),
(default, 'python', 'from sys import stdin

input1 = stdin.readline
n = input1()
ans = 0
a = "L"
i = 0
while i < len(n) - 1:
    if n[i] == "B":
        ans += 1
    elif n[i] == a == n[i + 1]:
        ans += 1
        if n[i] == "R":
            a = "L"
        else:
            a = "R"
        i += 1
    elif n[i] == a:
        ans += 1

    i += 1
print(ans)'),
(default, 'python', 'from sys import stdin

input1 = stdin.readline
n = input1()
ans = 0
a = "L"
i = 0
while i < len(n):
    if n[i] == "B":
        ans += 1
    elif n[i] == a == n[i + 1]:
        ans += 1
        if n[i] == "R":
            a = "L"
        else:
            a = "R"
        i += 1
    elif n[i] == a:
        ans += 1

    i += 1
print(ans)'),
(default, 'g++', '#include <iostream>
#include <vector>
#include <string>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> seq1(n);
    for (int i = 0; i < n; i++){
        cin >> seq1[i];
    }
    int m;
    cin >> m;
    vector<int> seq2(m);
    for (int i = 0; i < m; i++) {
        cin >> seq2[i];
    }
    vector<vector<int>> ans(n + 2, vector<int>(m + 2, 0));
    ans[1][1] = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (i + j > 2) {
                if (seq1[i - 1] == seq2[j - 1]) {
                    ans[i][j] = ans[i - 1][j - 1] + 1;
                } else {
                    ans[i][j] = max(ans[i - 1][j], ans[i][j - 1]);
                }
            }
        }
    }
    cout << ans[n][m];
    return 0;
}'),
(default, 'python', 'from sys import stdin

input1 = stdin.readline
n = input1()
ans = 0
a = "L"
i = 0
while i < len(n) - 1:
    if n[i] == "B":
        ans += 1
    elif n[i] == a == n[i + 1]:
        ans += 1
        if n[i] == "R":
            a = "L"
        else:
            a = "R"
        i += 1
    elif n[i] == a or i == len(n) - 2 and a != "R":
        ans += 1

    i += 1

print(ans)'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> mass(n);
    for (int i = 0; i < n; i++) {
        cin >> mass[i];
    }
    vector<vector<int>> ans(n + 1, vector<int>(0));
    ans[0].push_back(mass[0]);
    for (int i = 1; i < n; i++) {
        int cnt = n;
        for (int j = 0; j < i; j++) {
            if (mass[j] < mass[i] && ans[j].size() > ans[cnt].size()) {
                cnt = j;
            }
        }
        ans[i] = ans[cnt];
        ans[i].push_back(mass[i]);
    }
    int max_cnt = 0;
    int res = 0;
    for (int i = 0; i < n; i++) {
        if (max_cnt < ans[i].size()) {
            max_cnt = ans[i].size();
            res = i;
        }
    }
    for (auto x : ans[res]) {
        cout << x << " ";
    }
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>
#include <string>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> seq1(n);
    for (int i = 0; i < n; i++){
        cin >> seq1[i];
    }
    int m;
    cin >> m;
    vector<int> seq2(m);
    for (int i = 0; i < m; i++) {
        cin >> seq2[i];
    }
    vector<vector<int>> ans(n + 2, vector<int>(m + 2, 0));
    ans[1][1] = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (seq1[i - 1] == seq2[j - 1]) {
                ans[i][j] = ans[i - 1][j - 1] + 1;
            } else {
                ans[i][j] = max(ans[i - 1][j], ans[i][j - 1]);
            }
        }
    }
    cout << ans[n][m];
    return 0;
}'),
(default, 'g++', '#include <iostream>
#include <vector>

using namespace std;

int main() {
    const int MOD = 1e9 + 7;
    int n;
    cin >> n;
    vector<vector<long long>> pile(n + 1, vector<long long>(3));
    pile[1][0] = 1;
    pile[1][1] = 1;
    pile[1][2] = 1;
    for (int i = 2; i <= n; i++) {
        pile[i][0] = (pile[i - 1][1] + pile[i - 1][2]) % MOD;
        pile[i][1] = (pile[i - 1][0] + pile[i - 1][1] + pile[i - 1][2]) % MOD;
        pile[i][2] = pile[i][1];
    }
    cout << (pile[n][0] + pile[n][1] + pile[n][2]) % MOD;
    return 0;
}');
