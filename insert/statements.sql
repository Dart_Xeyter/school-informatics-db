insert into statement values
(default, '# Задача №3

---

### Контекст

Представьте, что вы работает в компании специализирующиеся на высокочастотной торговли. Текущий проект, на котором вы 
задействованы, предоставляет безопасную платформу для торгов в криптовалюте.

Непосредственно ваша текущая задача это миграция данных из одного хранилища в другое. Системой-источником выбран был 
[MongoDb](https://medium.com/nuances-of-programming/%D1%80%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE-%D0%BF%D0%BE-mongodb-6e844437b0de),
а система-приемник у вас Postgres. Архитекторами проекта было решено воспользоваться встроенными механизмами переноса данных, 
а не писать свой отдельный сервис.

### Постановка

В Postgres имеется 3 таблицы, в которые делается вставка:
 * `auctioneer`;
 * `attachment`;
 * `bet`.

Данные в источнике хранятся в коллекции `bettings`, примеры записей из этой коллекции:
```js
// запись № X:
{
    "auctioneer": {
        "firstname": "john",
        "lastname": "doe",
        "nickname": "mr.notknown",
        "email": "john.doe@email.com"
    },
    "attachment": {
        "filename": "key.token1",
        "location": {
            "datacenter": "AWS_EU",
            "localname": "iD1234sdv23r23rtfwv"
        }
    },
    "bet": {
        "volume": 0.00001,
        "ts": 1672520400
    }
}

// запись № (X+1):
{
    "auctioneer": {
        "firstname": "john",
        "lastname": "doe",
        "nickname": "mr.notknown",
        "email": "john.doe@email.com"
    },
    "attachment": {
        "filename": "key.token2",
        "location": {
            "datacenter": "AWS_US",
            "localname": "iDasd@-asfasf23@3s"
        }
    },
    "bet": {
        "volume": 0.00085,
        "ts": 1676928960
    }
}
```

**Небольшое объяснение**

Пользователи платформы (информация о пользователе — таблица `auctioneer`) делают ставки на аукционе
(таблица `bet`). Из соображений безопасности во время произведения ставки пользователь 
должен приложить файл, полученный во время регистрации на аукцион (таблица `attachment`).

**Технические ограничения**

Внутренний механизм миграции:
1. делает вставку БД по одному документу из коллекции;
2. не имеет прав на вставку в таблицы `auctioneer`, `attachment`, `bet`;
3. может вставлять документы "как есть" в виде строки.

В силу этих ограничений вставка происходит черех view `v_auction_payload`.

### Ожидаемый формат ответа

Ваше решение должно быть в виде дополнения к скрипту:
```postgresql
CREATE TABLE auctioneer (
    event_id   INTEGER,
    firstname  TEXT,
    lastname   TEXT,
    nickname   TEXT,
    email      TEXT
);
CREATE TABLE attachment (
    event_id   INTEGER,
    filename   TEXT,
    datacenter TEXT,
    localname  TEXT
);
CREATE TABLE bet (
    event_id   INTEGER,
    volume     NUMERIC,
    ts         TIMESTAMP
);

CREATE OR REPLACE FUNCTION auctioneer_to_json(event_id INTEGER)
    RETURNS TEXT
    LANGUAGE plpgsql
AS $$
DECLARE
    firstname_ TEXT;
    lastname_  TEXT;
    nickname_  TEXT;
    email_     TEXT;
BEGIN
    SELECT
        firstname, lastname, nickname, email
    INTO
        firstname_, lastname_, nickname_, email_
    FROM
        auctioneer
    WHERE
        auctioneer.event_id = $1;
    RETURN format(
        ''{"firstname": %I, "lastname": %I, "nickname": %I, "email": %I}'',
        firstname_, lastname_, nickname_, email_
    );
END;
$$;

CREATE OR REPLACE FUNCTION attachment_to_json(event_id INTEGER)
    RETURNS TEXT
    LANGUAGE plpgsql
AS $$
DECLARE
    filename_   TEXT;
    datacenter_ TEXT;
    localname_  TEXT;
BEGIN
    SELECT
        filename, datacenter, localname
    INTO
        filename_, datacenter_, localname_
    FROM
        attachment
    WHERE
        attachment.event_id = $1;
    RETURN format(
        ''{"filename": %I, location": {"datacenter": %I, "localname": %I}}'',
        filename_, datacenter_, localname_
    );
END;
$$;

CREATE OR REPLACE FUNCTION bet_to_json(event_id INTEGER)
    RETURNS TEXT
    LANGUAGE plpgsql
AS $$
DECLARE
    volume_     NUMERIC;
    ts_         TIMESTAMP;
BEGIN
    SELECT
        volume, ts
    INTO
        volume_, ts_
    FROM
        bet
    WHERE
        bet.event_id = $1;
    RETURN format(
        ''{"volume": %s, "ts": %s}'',
        volume_, EXTRACT(EPOCH FROM ts_)::BIGINT
    );
END;
$$;


CREATE OR REPLACE VIEW v_auction_payload(payload) AS
    SELECT
        format(
            ''{"auctioneer": %s, "attachment": %s, "bet": %s}'',
            auctioneer_to_json(bet.event_id),
            attachment_to_json(bet.event_id),
            bet_to_json(bet.event_id)
        )
    FROM
        auctioneer
    JOIN
        attachment
        ON
            auctioneer.event_id = attachment.event_id
    JOIN
        bet
        ON
            attachment.event_id = bet.event_id;
```

### Пример
Ваше дополнение к скрипту должно позволить получить следующий эффект
```postgresql
INSERT INTO v_auction_payload(payload) VALUES (
''{
    "auctioneer": {
        "firstname": "john",
        "lastname": "doe",
        "nickname": "mr.notknown",
        "email": "john.doe@email.com"
    },
    "attachment": {
        "filename": "key.token1",
        "location": {
            "datacenter": "AWS_EU",
            "localname": "iD1234sdv23r23rtfwv"
        }
    },
    "bet": {
        "volume": 0.00001,
        "ts": 1672520400
    }
}'');
INSERT INTO v_auction_payload(payload) VALUES (
''{
    "auctioneer": {
        "firstname": "john",
        "lastname": "doe",
        "nickname": "mr.notknown",
        "email": "john.doe@email.com"
    },
    "attachment": {
        "filename": "key.token2",
        "location": {
            "datacenter": "AWS_US",
            "localname": "iDasd@-asfasf23@3s"
        }
    },
    "bet": {
        "volume": 0.00085,
        "ts": 1676928960
    }
}'');
```
```postgresql
SELECT * FROM auctioneer;

--

1,john,doe,mr.notknown,john.doe@email.com
2,john,doe,mr.notknown,john.doe@email.com
```
```postgresql
SELECT * FROM attachment;

---

1,key.token1,AWS_EU,iD1234sdv23r23rtfwv
2,key.token2,AWS_US,iDasd@-asfasf23@3s
```
```postgresql
SELECT * FROM bet;

---

1,0.00001,2022-12-31 21:00:00.000000
2,0.00085,2023-02-20 21:36:00.000000
```
```postgresql
SELECT * FROM v_auction_payload;

---

"{""auctioneer"": {""firstname"": john, ""lastname"": doe, ""nickname"": ""mr.notknown"", ""email"": ""john.doe@email.com""}, ""attachment"": {""filename"": ""key.token1"", location"": {""datacenter"": ""AWS_EU"", ""localname"": ""iD1234sdv23r23rtfwv""}}, ""bet"": {""volume"": 0.00001, ""ts"": 1672520400}}"
"{""auctioneer"": {""firstname"": john, ""lastname"": doe, ""nickname"": ""mr.notknown"", ""email"": ""john.doe@email.com""}, ""attachment"": {""filename"": ""key.token2"", location"": {""datacenter"": ""AWS_US"", ""localname"": ""iDasd@-asfasf23@3s""}}, ""bet"": {""volume"": 0.00085, ""ts"": 1676928960}}"
```

### Подсказки
 * [Ознакомтесь с работой с JSON в Postgres](https://www.postgresql.org/docs/9.3/functions-json.html)
 * Для нумерации событий стоит воспользоваться с ранее изученным объектом из предыдущего задания.
 * Нумерация должна начинаться с 1.
 * Для получения текущего момента времени можно воспользоваться функцией `now()`.', null),
(default, '# Задача №2

---

### Контекст

В PostgreSQL существует объекты называемые `sequence`. 
`CREATE SEQUENCE` создаёт генератор последовательности. Эта операция включает создание и инициализацию специальной таблицы _имя_, содержащей одну строку.
```postgresql
CREATE [ TEMPORARY | TEMP ] SEQUENCE [ IF NOT EXISTS ] имя [ INCREMENT [ BY ] шаг ]
    [ MINVALUE мин_значение | NO MINVALUE ] [ MAXVALUE макс_значение | NO MAXVALUE ]
    [ START [ WITH ] начало ] [ CACHE кеш ] [ [ NO ] CYCLE ]
    [ OWNED BY { имя_таблицы.имя_столбца | NONE } ]
```
**Пример:**
Создание возрастающей последовательности с именем `serial`, с начальным значением 101:
```postgresql
CREATE SEQUENCE serial START 101;
```
Получение следующего номера этой последовательности:
```postgresql
SELECT nextval(''serial'');

 nextval
---------
     101
```
Получение следующего номера этой последовательности:
```postgresql
SELECT nextval(''serial'');

 nextval
---------
     102
```

После создания последовательности работать с ней можно, вызывая функции `nextval`, `currval` и `setval`.

Использование сиквенсов для генерации таблички довольно непростая задача. Давайте в рамках этого задания напишем функцию,
которая сгенерирует табличку из последовательных чисел.

### Постановка

Написать функцию `serial_generator(start_val_inc INTEGER, last_val_ex INTEGER)`, которая возвращает таблицу из последовательных
чисел с шагом `+1`. Скрипт создания должен быть нечувствителен к уже объявленным функциям. 

**Пример**
```postgresql
SELECT * FROM serial_generator(10, 12);

 serial_generator
------------------
            10
            11
```

**Дополнительные условия**
 * функция должна быть написана на `plpgsql`;
 * функция генерирует значения начиная с `start_val_inc` до тех пор, пока оно строго меньше `last_val_ex`;
 * **запрещено** использовать функцию `generate_series`;
 * самостоятельно изучите документацию как _генерировать_ табличку из функции.

### Ожидаемый формат ответа

Выводить ничего не надо. Скрипт с решением должен содержать __только__ объявление функции.
', null),
(default, '# Задача №1

---

### Контекст

В схеме `public` имеется таблица с историей криптовалюты.

```postgresql
CREATE TABLE IF NOT EXISTS coins(
    dt VARCHAR(16),
    avg_price NUMERIC,
    tx_cnt NUMERIC,
    tx_vol NUMERIC,
    active_addr_cnt NUMERIC,
    symbol VARCHAR(8),
    full_nm VARCHAR(128),
    open_price NUMERIC,
    high_price NUMERIC,
    low_price NUMERIC,
    close_price NUMERIC,
    vol NUMERIC,
    market NUMERIC
)
```

Поясним значения хранящиеся в колонках:

* `dt` — дата измерений
* `avg_price` — средняя цена монеты за торговый день в USD
* `tx_cnt` — количество транзакций в сети данной монеты
* `tx_vol` — объем монет переведенных между адресами в сети данной монеты
* `active_addr_cnt` — количество адресов совершавших а данный день транзации в сети данной монеты
* `symbol` — сокращенное название монеты
* `full_nm` — полное название монеты
* `open_price` — цена монеты в начале торгов данного дня
* `high_price` — самая высокая цена данной монеты в течение данного торгового дня
* `low_price` — самая низкая цена данной монеты в течение данного торгового дня
* `close_price` — цена монеты в конце торгов данного дня
* `vol` — объем торгов данной монетой на биржах в данный день
* `market` — капитализация данной монеты в данный день

### Постановка

Написать функцию `count_non_volatile_days(full_nm TEXT)`, которая считает кол-во дней, когда цена торгов криптомонеты не менялась.
Если такой криптомонеты не существует, то следует вызвать исключение с текстом: `Crypto currency with name "{full_nm}" is absent in database!`, 
`{full_nm}` — имя криптомонеты, взятое из аргументов функции. Исключение должно иметь `ERRCODE = ''02000''`. Скрипт создания 
должен быть нечувствителен к уже объявленным функциям. Функция должна быть написана на `plpgsql`.


### Ожидаемый формат ответа

Выводить ничего не надо. Скрипт с решением должен содержать __только__ объявление функции.
', null),
(default, '# Задача №9

---

### Контекст

В схеме `public` имеется таблица с историей криптовалюты.

```postgresql
CREATE TABLE IF NOT EXISTS coins(
    dt VARCHAR(16),
    avg_price NUMERIC,
    tx_cnt NUMERIC,
    tx_vol NUMERIC,
    active_addr_cnt NUMERIC,
    symbol VARCHAR(8),
    full_nm VARCHAR(128),
    open_price NUMERIC,
    high_price NUMERIC,
    low_price NUMERIC,
    close_price NUMERIC,
    vol NUMERIC,
    market NUMERIC
)
```

Поясним значения хранящиеся в колонках:

* `dt` — дата измерений
* `avg_price` — средняя цена монеты за торговый день в USD
* `tx_cnt` — количество транзакций в сети данной монеты
* `tx_vol` — объем монет переведенных между адресами в сети данной монеты
* `active_addr_cnt` — количество адресов совершавших а данный день транзации в сети данной монеты
* `symbol` — сокращенное название монеты
* `full_nm` — полное название монеты
* `open_price` — цена монеты в начале торгов данного дня
* `high_price` — самая высокая цена данной монеты в течение данного торгового дня
* `low_price` — самая низкая цена данной монеты в течение данного торгового дня
* `close_price` — цена монеты в конце торгов данного дня
* `vol` — объем торгов данной монетой на биржах в данный день
* `market` - капитализация данной монеты в данный день

### Постановка

Удалить из таблицы записи, где не было транзакций или торгов.  

Прим.: использовать оператор `DELETE`.

### Ожидаемый формат ответа

Выводить ничего не надо.
', null),
(default, '# Задача №3

---

### Контекст

В схеме `public` имеется таблица с измерениями роста (в дюймах) и веса (в фунтах) из одного исследования в США.

```postgresql
CREATE TABLE IF NOT EXISTS hw(
    id INTEGER,
    height FLOAT4,
    weight FLOAT4
)
```

### Постановка

Найдите количество людей, с дефицитом массы тела (`BMI < 18,5`).

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| underweight_count |
|-------------------|
| 1024              |

', null),
(default, '# Задача №8

---

### Контекст

В схеме `public` имеется таблица с историей криптовалюты.

```postgresql
CREATE TABLE IF NOT EXISTS coins(
    dt VARCHAR(16),
    avg_price NUMERIC,
    tx_cnt NUMERIC,
    tx_vol NUMERIC,
    active_addr_cnt NUMERIC,
    symbol VARCHAR(8),
    full_nm VARCHAR(128),
    open_price NUMERIC,
    high_price NUMERIC,
    low_price NUMERIC,
    close_price NUMERIC,
    vol NUMERIC,
    market NUMERIC
)
```

Описание значений, хранящихся в колонках:

* `dt` — дата измерений
* `avg_price` — средняя цена монеты за торговый день в USD
* `tx_cnt` — количество транзакций в сети данной монеты
* `tx_vol` — объем монет переведенных между адресами в сети данной монеты
* `active_addr_cnt` — количество адресов совершавших а данный день транзации в сети данной монеты
* `symbol` — сокращенное название монеты
* `full_nm` — полное название монеты
* `open_price` — цена монеты в начале торгов данного дня
* `high_price` — самая высокая цена данной монеты в течение данного торгового дня
* `low_price` — самая низкая цена данной монеты в течение данного торгового дня
* `close_price` — цена монеты в конце торгов данного дня
* `vol` — объем торгов данной монетой на биржах в данный день
* `market` - капитализация данной монеты в данный день

### Постановка

Вывести для каждой криптомонеты день, когда значение цены было самым высоким. 
Упорядочить результат по цене в порядке убывания, а в случае равенства цен упорядочить по имени монеты. 
Если имеется несколько дней с таким значением, то выбираем самый ранний. 
Имя криптовалюты ожидается в верхнем регистре.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| full_name | dt         | price   |
|-----------|------------|---------|
| BITCOIN   | 2018-12-31 | 60000.0 |
| ...       | ...        | ...     |
', null),
(default, '# Задача №10

---

### Контекст

Имеется база данных для недавно созданного загородного клуба. В ней имеется информация о членах этого клуба, объектов для 
отдыха, таких как теннисные корты, и истории бронирования. Помимо прочего, клуб хочет понять, как они могут использовать 
свою информацию для анализа использования/спроса на объекты. __Обратите внимание__: этот набор данных предназначен 
исключительно для интересного набора упражнений, а схема базы данных несовершенна в нескольких аспектах — пожалуйста, не
воспринимайте ее как пример хорошего дизайна.

В БД в схеме `cd` имееются 3 таблицы. Для этого задания нам потребуется только одна таблица `cd.facilities`.   

---

**Таблица `cd.facilities`**

```postgresql
CREATE TABLE cd.facilities(
   facid               INTEGER                NOT NULL, 
   name                CHARACTER VARYING(100) NOT NULL, 
   membercost          NUMERIC                NOT NULL, 
   guestcost           NUMERIC                NOT NULL, 
   initialoutlay       NUMERIC                NOT NULL, 
   monthlymaintenance  NUMERIC                NOT NULL
);
```

В таблице перечислены все доступные для бронирования объекты, которыми располагает загородный клуб. Клуб хранит 
информацию об идентификаторе/имени, стоимости бронирования как членов, так и гостей, первоначальную стоимость строительства объекта и предполагаемые ежемесячные расходы на содержание.

---

### Постановка

Выведите названия объектов и их ценовую категорию(объект получает ценовую категорию "expensive", если его ежемесячная стоимость обслуживания превышает 100 долларов, и "cheap" в ином случае).



### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| name             | cost      |
|------------------|-----------|
| Tennis Court 1   | expensive |
| Tennis Court 2   | expensive |
| Badminton Court  | cheap     |
| ...              | ...       |
', null),
(default, '# Задача №4

---

### Контекст

В схеме `public` имеется таблица с измерениями роста (в дюймах) и веса (в фунтах) из одного исследования в США.

```postgresql
CREATE TABLE IF NOT EXISTS hw(
    id INTEGER,
    height FLOAT4,
    weight FLOAT4
)
```

### Постановка

Упорядочить людей по их `BMI` в убывающем порядке, а также указать тип комплекции согласно их `BMI`:
 * `BMI < 18,5` — `underweight`
 * `18,5 <= BMI < 25` — `normal`
 * `25 <= BMI < 30` — `overweight`
 * `30 <= BMI < 35` — `obese`
 * `35 <= BMI` — `extremely obese`

В случае равенства BMI упорядочивать по `id` также в порядке убывания.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| id  | bmi  | type   |
|-----|------|--------|
| 1   | 23.9 | normal |
| ... | .... | .......|

', null),
(default, '# Задача №2

---

### Контекст

В схеме `public` имеется таблица с измерениями роста (в дюймах) и веса (в фунтах) из одного исследования в США.

```postgresql
CREATE TABLE IF NOT EXISTS hw(
    id INTEGER,
    height FLOAT4,
    weight FLOAT4
)
```

### Постановка

Посчитайте BMI (Body Mass Index — [link](https://www.cdc.gov/healthyweight/assessing/bmi/adult_bmi/index.html#:~:text=Body%20mass%20index%20(BMI)%20is,weight%2C%20overweight%2C%20and%20obesity.)) 
для каждого измерения.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| bmi     | 
|---------|
| 18.5    |
| 23.2637 |
| 33.042  |
| 21.901  |
', null),
(default, '# Задача №7

---

### Контекст

В схеме `public` имеется таблица с историей криптовалюты.

```postgresql
CREATE TABLE IF NOT EXISTS coins(
    dt VARCHAR(16),
    avg_price NUMERIC,
    tx_cnt NUMERIC,
    tx_vol NUMERIC,
    active_addr_cnt NUMERIC,
    symbol VARCHAR(8),
    full_nm VARCHAR(128),
    open_price NUMERIC,
    high_price NUMERIC,
    low_price NUMERIC,
    close_price NUMERIC,
    vol NUMERIC,
    market NUMERIC
)
```

Описание значений, хранящихся в колонках:

* `dt` — дата измерений
* `avg_price` — средняя цена монеты за торговый день в USD
* `tx_cnt` — количество транзакций в сети данной монеты
* `tx_vol` — объем монет переведенных между адресами в сети данной монеты
* `active_addr_cnt` — количество адресов совершавших а данный день транзации в сети данной монеты
* `symbol` — сокращенное название монеты
* `full_nm` — полное название монеты
* `open_price` — цена монеты в начале торгов данного дня
* `high_price` — самая высокая цена данной монеты в течение данного торгового дня
* `low_price` — самая низкая цена данной монеты в течение данного торгового дня
* `close_price` — цена монеты в конце торгов данного дня
* `vol` — объем торгов данной монетой на биржах в данный день
* `market` - капитализация данной монеты в данный день

### Постановка

Вывести полные названия всех криптомонет в верхнем регистре без дубликатов. Результат должен быть отсортирован в лексикографическом порядке.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| full_name |
|-----------|
| BITCOIN   |
| ...       |
', null),
(default, '# Задача №5

---

### Контекст

В схеме `public` имеется таблица, в которой хранится сводная статистика по операциям и параметрам торгов криптовалютами в разрезе дней.

```postgresql
CREATE TABLE IF NOT EXISTS coins(
    dt VARCHAR(16),
    avg_price NUMERIC,
    tx_cnt NUMERIC,
    tx_vol NUMERIC,
    active_addr_cnt NUMERIC,
    symbol VARCHAR(8),
    full_nm VARCHAR(128),
    open_price NUMERIC,
    high_price NUMERIC,
    low_price NUMERIC,
    close_price NUMERIC,
    vol NUMERIC,
    market NUMERIC
)
```

Описание значений, хранящихся в колонках:

 * `dt` — дата измерений
 * `avg_price` — средняя цена монеты за торговый день в USD
 * `tx_cnt` — количество транзакций в сети данной монеты
 * `tx_vol` — объем монет переведенных между адресами в сети данной монеты
 * `active_addr_cnt` — количество адресов совершавших а данный день транзации в сети данной монеты
 * `symbol` — сокращенное название монеты
 * `full_nm` — полное название монеты
 * `open_price` — цена монеты в начале торгов данного дня
 * `high_price` — самая высокая цена данной монеты в течение данного торгового дня
 * `low_price` — самая низкая цена данной монеты в течение данного торгового дня
 * `close_price` — цена монеты в конце торгов данного дня
 * `vol` — объем торгов данной монетой на биржах в данный день
 * `market` - капитализация данной монеты в данный день

### Постановка

Выбрать из таблицы все записи про биткоин (''BTC''), где его средняя цена за торговый день меньше 100 USD.

### Ожидаемый формат ответа

Совпадает со структурой исходной таблицы (выводятся все колонки таблицы).
', null),
(default, '# Задача №1

---

### Контекст

В схеме `public` имеется таблица с измерениями роста (в дюймах) и веса (в фунтах) из одного исследования в США.

```postgresql
CREATE TABLE IF NOT EXISTS hw(
    id INTEGER,
    height FLOAT4,
    weight FLOAT4
)
```

### Постановка

Найдите значения наименьшего и наибольшего роста и веса.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| min_height | max_height | min_weight | max_weight |
|------------|------------|------------|------------|
| 23.02      | 63.12      | 98.01123   | 230.24     |

Числовые значения **не надо** конвертировать в метрическую систему.
', null),
(default, '# Задача №6

---

### Контекст

В схеме `public` имеется таблица с историей криптовалюты.

```postgresql
CREATE TABLE IF NOT EXISTS coins(
    dt VARCHAR(16),
    avg_price NUMERIC,
    tx_cnt NUMERIC,
    tx_vol NUMERIC,
    active_addr_cnt NUMERIC,
    symbol VARCHAR(8),
    full_nm VARCHAR(128),
    open_price NUMERIC,
    high_price NUMERIC,
    low_price NUMERIC,
    close_price NUMERIC,
    vol NUMERIC,
    market NUMERIC
)
```

Описание значений, хранящихся в колонках:

* `dt` — дата измерений
* `avg_price` — средняя цена монеты за торговый день в USD
* `tx_cnt` — количество транзакций в сети данной монеты
* `tx_vol` — объем монет переведенных между адресами в сети данной монеты
* `active_addr_cnt` — количество адресов совершавших а данный день транзации в сети данной монеты
* `symbol` — сокращенное название монеты
* `full_nm` — полное название монеты
* `open_price` — цена монеты в начале торгов данного дня
* `high_price` — самая высокая цена данной монеты в течение данного торгового дня
* `low_price` — самая низкая цена данной монеты в течение данного торгового дня
* `close_price` — цена монеты в конце торгов данного дня
* `vol` — объем торгов данной монетой на биржах в данный день
* `market` - капитализация данной монеты в данный день

### Постановка

Выбрать из данных по крипотвалюте `DOGE` за 2018 год самую высокую цену и объем торгов за дни, когда её средняя цена была выше 0.1 цента.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| dt         | high_price | vol   |
|------------|------------|-------|
| 2022-02-02 | 23.02      | 63.12 |
| ...        | ...        | ...   |
', null),
(default, '# Задача №9

---

### Контекст

Имеется база данных для недавно созданного загородного клуба. В ней имеется информация о членах этого клуба, объектов для 
отдыха, таких как теннисные корты, и истории бронирования. Помимо прочего, клуб хочет понять, как они могут использовать 
свою информацию для анализа использования/спроса на объекты. __Обратите внимание__: этот набор данных предназначен 
исключительно для интересного набора упражнений, а схема базы данных несовершенна в нескольких аспектах — пожалуйста, не
воспринимайте ее как пример хорошего дизайна.

В БД в схеме `cd` имееются 3 таблицы.  

---

**Таблица `cd.members`**

```postgresql
CREATE TABLE cd.members(
    memid          INTEGER                NOT NULL,
    surname        CHARACTER VARYING(200) NOT NULL,
    firstname      CHARACTER VARYING(200) NOT NULL,
    address        CHARACTER VARYING(300) NOT NULL,
    zipcode        INTEGER                NOT NULL,
    telephone      CHARACTER VARYING(20)  NOT NULL,
    recommendedby  INTEGER,
    joindate       TIMESTAMP              NOT NULL,
    
    CONSTRAINT members_pk PRIMARY KEY (memid),
    
    CONSTRAINT fk_members_recommendedby FOREIGN KEY (recommendedby)
        REFERENCES cd.members(memid) ON DELETE SET NULL
);
```

У каждого участника есть идентификатор (не обязательно последовательный), основная информация об адресе, ссылка на 
участника, который рекомендовал их (если есть), и отметка времени, когда они присоединились.

---

**Таблица `cd.facilities`**

```postgresql
CREATE TABLE cd.facilities(
   facid               INTEGER                NOT NULL, 
   name                CHARACTER VARYING(100) NOT NULL, 
   membercost          NUMERIC                NOT NULL, 
   guestcost           NUMERIC                NOT NULL, 
   initialoutlay       NUMERIC                NOT NULL, 
   monthlymaintenance  NUMERIC                NOT NULL, 
   
   CONSTRAINT facilities_pk PRIMARY KEY (facid)
);
```

В таблице перечислены все доступные для бронирования объекты, которыми располагает загородный клуб. Клуб хранит 
информацию об идентификаторе/имени, стоимости бронирования как членов, так и гостей, первоначальную стоимость строительства объекта и предполагаемые ежемесячные расходы на содержание.

---

**Таблица `cd.bookings`**

```postgresql
CREATE TABLE cd.bookings(
   bookid     INTEGER   NOT NULL, 
   facid      INTEGER   NOT NULL, 
   memid      INTEGER   NOT NULL, 
   starttime  TIMESTAMP NOT NULL,
   slots      INTEGER   NOT NULL,
   
   CONSTRAINT bookings_pk PRIMARY KEY (bookid),
   
   CONSTRAINT fk_bookings_facid FOREIGN KEY (facid) REFERENCES cd.facilities(facid),
   
   CONSTRAINT fk_bookings_memid FOREIGN KEY (memid) REFERENCES cd.members(memid)
);
```

И таблица, отслеживающая бронирование объектов. В нем хранится идентификатор объекта, член, который сделал бронирование,
начало бронирования и количество получасовых «слотов», на которые было сделано бронирование.

---

Схематически это выглядит примерно так:
![](./img/schema-horizontal.png)

### Постановка

Как вы можете составить список бронирований на `2012-09-14`, которые обойдутся участнику (или гостю) более чем в 30 
долларов? Помните, что гости имеют разные затраты по сравнению с участниками (перечисленные расходы указаны за 
получасовой «слот»), а гостевой пользователь всегда имеет идентификатор 0. Включите в свой вывод название объекта, 
имя члена, отформатированное как одно колонка и стоимость. Упорядочить по убыванию стоимости, в случае равенства 
отсортировать по `member` и `facility` лексикографически. 
**Решение не должно содержать ни одного соединения (`[INNER/LEFT OUTER/RIGHT OUTER/FULL] JOIN`, декартово соединение также).**

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| member      | facility       | cost |
|-------------|----------------|------|
| Anne Baker  | Tennis Court 1 | 300  |
| Anne Baker  | Tennis Court 2 | 210  |
| GUEST GUEST | Tennis Court 1 | 120  | 
| ...         | ...            |      |
', null),
(default, '# Задача №3

---

### Контекст

Имеется база данных для недавно созданного загородного клуба. В ней имеется информация о членах этого клуба, объектов для 
отдыха, таких как теннисные корты, и истории бронирования. Помимо прочего, клуб хочет понять, как они могут использовать 
свою информацию для анализа использования/спроса на объекты. __Обратите внимание__: этот набор данных предназначен 
исключительно для интересного набора упражнений, а схема базы данных несовершенна в нескольких аспектах — пожалуйста, не
воспринимайте ее как пример хорошего дизайна.

В БД в схеме `cd` имееются 3 таблицы.  

---

**Таблица `cd.members`**

```postgresql
CREATE TABLE cd.members(
    memid          INTEGER                NOT NULL,
    surname        CHARACTER VARYING(200) NOT NULL,
    firstname      CHARACTER VARYING(200) NOT NULL,
    address        CHARACTER VARYING(300) NOT NULL,
    zipcode        INTEGER                NOT NULL,
    telephone      CHARACTER VARYING(20)  NOT NULL,
    recommendedby  INTEGER,
    joindate       TIMESTAMP              NOT NULL,
    
    CONSTRAINT members_pk PRIMARY KEY (memid),
    
    CONSTRAINT fk_members_recommendedby FOREIGN KEY (recommendedby)
        REFERENCES cd.members(memid) ON DELETE SET NULL
);
```

У каждого участника есть идентификатор (не обязательно последовательный), основная информация об адресе, ссылка на 
участника, который рекомендовал их (если есть), и отметка времени, когда они присоединились.

---

**Таблица `cd.facilities`**

```postgresql
CREATE TABLE cd.facilities(
   facid               INTEGER                NOT NULL, 
   name                CHARACTER VARYING(100) NOT NULL, 
   membercost          NUMERIC                NOT NULL, 
   guestcost           NUMERIC                NOT NULL, 
   initialoutlay       NUMERIC                NOT NULL, 
   monthlymaintenance  NUMERIC                NOT NULL, 
   
   CONSTRAINT facilities_pk PRIMARY KEY (facid)
);
```

В таблице перечислены все доступные для бронирования объекты, которыми располагает загородный клуб. Клуб хранит 
информацию об идентификаторе/имени, стоимости бронирования как членов, так и гостей, первоначальную стоимость строительства объекта и предполагаемые ежемесячные расходы на содержание.

---

**Таблица `cd.bookings`**

```postgresql
CREATE TABLE cd.bookings(
   bookid     INTEGER   NOT NULL, 
   facid      INTEGER   NOT NULL, 
   memid      INTEGER   NOT NULL, 
   starttime  TIMESTAMP NOT NULL,
   slots      INTEGER   NOT NULL,
   
   CONSTRAINT bookings_pk PRIMARY KEY (bookid),
   
   CONSTRAINT fk_bookings_facid FOREIGN KEY (facid) REFERENCES cd.facilities(facid),
   
   CONSTRAINT fk_bookings_memid FOREIGN KEY (memid) REFERENCES cd.members(memid)
);
```

И таблица, отслеживающая бронирование объектов. В нем хранится идентификатор объекта, член, который сделал бронирование,
начало бронирования и количество получасовых «слотов», на которые было сделано бронирование.

---

Схематически это выглядит примерно так:
![](./img/schema-horizontal.png)

### Постановка

Создайте список общего количества слотов, забронированных на объект в месяц в 2012 году. Создайте выходную таблицу, состоящую из идентификатора объекта и слотов, отсортированных по идентификатору и месяцу.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| facid | month | total_slots |
|-------|-------|-------------|
| 0     | 7     | 270         | 
| 0     | 8     | 459         | 
| ...   | ...   | ...         | 
', null),
(default, '# Задача №8

---

### Контекст

Имеется база данных для недавно созданного загородного клуба. В ней имеется информация о членах этого клуба, объектов для 
отдыха, таких как теннисные корты, и истории бронирования. Помимо прочего, клуб хочет понять, как они могут использовать 
свою информацию для анализа использования/спроса на объекты. __Обратите внимание__: этот набор данных предназначен 
исключительно для интересного набора упражнений, а схема базы данных несовершенна в нескольких аспектах — пожалуйста, не
воспринимайте ее как пример хорошего дизайна.

В БД в схеме `cd` имееются 3 таблицы.  

---

**Таблица `cd.members`**

```postgresql
CREATE TABLE cd.members(
    memid          INTEGER                NOT NULL,
    surname        CHARACTER VARYING(200) NOT NULL,
    firstname      CHARACTER VARYING(200) NOT NULL,
    address        CHARACTER VARYING(300) NOT NULL,
    zipcode        INTEGER                NOT NULL,
    telephone      CHARACTER VARYING(20)  NOT NULL,
    recommendedby  INTEGER,
    joindate       TIMESTAMP              NOT NULL,
    
    CONSTRAINT members_pk PRIMARY KEY (memid),
    
    CONSTRAINT fk_members_recommendedby FOREIGN KEY (recommendedby)
        REFERENCES cd.members(memid) ON DELETE SET NULL
);
```

У каждого участника есть идентификатор (не обязательно последовательный), основная информация об адресе, ссылка на 
участника, который рекомендовал их (если есть), и отметка времени, когда они присоединились.

---

**Таблица `cd.facilities`**

```postgresql
CREATE TABLE cd.facilities(
   facid               INTEGER                NOT NULL, 
   name                CHARACTER VARYING(100) NOT NULL, 
   membercost          NUMERIC                NOT NULL, 
   guestcost           NUMERIC                NOT NULL, 
   initialoutlay       NUMERIC                NOT NULL, 
   monthlymaintenance  NUMERIC                NOT NULL, 
   
   CONSTRAINT facilities_pk PRIMARY KEY (facid)
);
```

В таблице перечислены все доступные для бронирования объекты, которыми располагает загородный клуб. Клуб хранит 
информацию об идентификаторе/имени, стоимости бронирования как членов, так и гостей, первоначальную стоимость строительства объекта и предполагаемые ежемесячные расходы на содержание.

---

**Таблица `cd.bookings`**

```postgresql
CREATE TABLE cd.bookings(
   bookid     INTEGER   NOT NULL, 
   facid      INTEGER   NOT NULL, 
   memid      INTEGER   NOT NULL, 
   starttime  TIMESTAMP NOT NULL,
   slots      INTEGER   NOT NULL,
   
   CONSTRAINT bookings_pk PRIMARY KEY (bookid),
   
   CONSTRAINT fk_bookings_facid FOREIGN KEY (facid) REFERENCES cd.facilities(facid),
   
   CONSTRAINT fk_bookings_memid FOREIGN KEY (memid) REFERENCES cd.members(memid)
);
```

И таблица, отслеживающая бронирование объектов. В нем хранится идентификатор объекта, член, который сделал бронирование,
начало бронирования и количество получасовых «слотов», на которые было сделано бронирование.

---

Схематически это выглядит примерно так:
![](./img/schema-horizontal.png)

### Постановка

Как составить список всех участников, которые пользовались теннисным кортом? Включите в свой вывод название суда и имя члена, отформатированное как один столбец. Убедитесь, что данные не повторяются, и упорядочьте имя участника, за которым следует название объекта.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| member        | facility       |
|---------------|----------------|
| Anne Baker    | Tennis Court 1 |
| Anne Baker    | Tennis Court 2 |
| Burton Tracy  | Tennis Court 1 |
| ...           | ...            |

', null),
(default, '# Задача №10

---

### Контекст

Имеется база данных для недавно созданного загородного клуба. В ней имеется информация о членах этого клуба, объектов для 
отдыха, таких как теннисные корты, и истории бронирования. Помимо прочего, клуб хочет понять, как они могут использовать 
свою информацию для анализа использования/спроса на объекты. __Обратите внимание__: этот набор данных предназначен 
исключительно для интересного набора упражнений, а схема базы данных несовершенна в нескольких аспектах — пожалуйста, не
воспринимайте ее как пример хорошего дизайна.

В БД в схеме `cd` имееются 3 таблицы.  

---

**Таблица `cd.members`**

```postgresql
CREATE TABLE cd.members(
    memid          INTEGER                NOT NULL,
    surname        CHARACTER VARYING(200) NOT NULL,
    firstname      CHARACTER VARYING(200) NOT NULL,
    address        CHARACTER VARYING(300) NOT NULL,
    zipcode        INTEGER                NOT NULL,
    telephone      CHARACTER VARYING(20)  NOT NULL,
    recommendedby  INTEGER,
    joindate       TIMESTAMP              NOT NULL,
    
    CONSTRAINT members_pk PRIMARY KEY (memid),
    
    CONSTRAINT fk_members_recommendedby FOREIGN KEY (recommendedby)
        REFERENCES cd.members(memid) ON DELETE SET NULL
);
```

У каждого участника есть идентификатор (не обязательно последовательный), основная информация об адресе, ссылка на 
участника, который рекомендовал их (если есть), и отметка времени, когда они присоединились.

---

**Таблица `cd.facilities`**

```postgresql
CREATE TABLE cd.facilities(
   facid               INTEGER                NOT NULL, 
   name                CHARACTER VARYING(100) NOT NULL, 
   membercost          NUMERIC                NOT NULL, 
   guestcost           NUMERIC                NOT NULL, 
   initialoutlay       NUMERIC                NOT NULL, 
   monthlymaintenance  NUMERIC                NOT NULL, 
   
   CONSTRAINT facilities_pk PRIMARY KEY (facid)
);
```

В таблице перечислены все доступные для бронирования объекты, которыми располагает загородный клуб. Клуб хранит 
информацию об идентификаторе/имени, стоимости бронирования как членов, так и гостей, первоначальную стоимость строительства объекта и предполагаемые ежемесячные расходы на содержание.

---

**Таблица `cd.bookings`**

```postgresql
CREATE TABLE cd.bookings(
   bookid     INTEGER   NOT NULL, 
   facid      INTEGER   NOT NULL, 
   memid      INTEGER   NOT NULL, 
   starttime  TIMESTAMP NOT NULL,
   slots      INTEGER   NOT NULL,
   
   CONSTRAINT bookings_pk PRIMARY KEY (bookid),
   
   CONSTRAINT fk_bookings_facid FOREIGN KEY (facid) REFERENCES cd.facilities(facid),
   
   CONSTRAINT fk_bookings_memid FOREIGN KEY (memid) REFERENCES cd.members(memid)
);
```

И таблица, отслеживающая бронирование объектов. В нем хранится идентификатор объекта, член, который сделал бронирование,
начало бронирования и количество получасовых «слотов», на которые было сделано бронирование.

---

Схематически это выглядит примерно так:
![](./img/schema-horizontal.png)

### Постановка

Как вывести список всех участников, включая тех, кто их порекомендовал (если таковые имеются), без использования каких-либо объединений? Убедитесь, что каждая пара имя + фамилия отформатирована как столбец и упорядочена.
**Решение не должно содержать ни одного соединения (`[INNER/LEFT OUTER/RIGHT OUTER/FULL] JOIN`, декартово соединение также).**

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| member         | recommender  |
|----------------|--------------|
| Anna Mackenzie | Burton Tracy |
| Burton Tracy   | NULL         |
| ...            | ...          |
', null),
(default, '# Задача №4

---

### Контекст

Имеется база данных для недавно созданного загородного клуба. В ней имеется информация о членах этого клуба, объектов для 
отдыха, таких как теннисные корты, и истории бронирования. Помимо прочего, клуб хочет понять, как они могут использовать 
свою информацию для анализа использования/спроса на объекты. __Обратите внимание__: этот набор данных предназначен 
исключительно для интересного набора упражнений, а схема базы данных несовершенна в нескольких аспектах — пожалуйста, не
воспринимайте ее как пример хорошего дизайна.

В БД в схеме `cd` имееются 3 таблицы.  

---

**Таблица `cd.members`**

```postgresql
CREATE TABLE cd.members(
    memid          INTEGER                NOT NULL,
    surname        CHARACTER VARYING(200) NOT NULL,
    firstname      CHARACTER VARYING(200) NOT NULL,
    address        CHARACTER VARYING(300) NOT NULL,
    zipcode        INTEGER                NOT NULL,
    telephone      CHARACTER VARYING(20)  NOT NULL,
    recommendedby  INTEGER,
    joindate       TIMESTAMP              NOT NULL,
    
    CONSTRAINT members_pk PRIMARY KEY (memid),
    
    CONSTRAINT fk_members_recommendedby FOREIGN KEY (recommendedby)
        REFERENCES cd.members(memid) ON DELETE SET NULL
);
```

У каждого участника есть идентификатор (не обязательно последовательный), основная информация об адресе, ссылка на 
участника, который рекомендовал их (если есть), и отметка времени, когда они присоединились.

---

**Таблица `cd.facilities`**

```postgresql
CREATE TABLE cd.facilities(
   facid               INTEGER                NOT NULL, 
   name                CHARACTER VARYING(100) NOT NULL, 
   membercost          NUMERIC                NOT NULL, 
   guestcost           NUMERIC                NOT NULL, 
   initialoutlay       NUMERIC                NOT NULL, 
   monthlymaintenance  NUMERIC                NOT NULL, 
   
   CONSTRAINT facilities_pk PRIMARY KEY (facid)
);
```

В таблице перечислены все доступные для бронирования объекты, которыми располагает загородный клуб. Клуб хранит 
информацию об идентификаторе/имени, стоимости бронирования как членов, так и гостей, первоначальную стоимость строительства объекта и предполагаемые ежемесячные расходы на содержание.

---

**Таблица `cd.bookings`**

```postgresql
CREATE TABLE cd.bookings(
   bookid     INTEGER   NOT NULL, 
   facid      INTEGER   NOT NULL, 
   memid      INTEGER   NOT NULL, 
   starttime  TIMESTAMP NOT NULL,
   slots      INTEGER   NOT NULL,
   
   CONSTRAINT bookings_pk PRIMARY KEY (bookid),
   
   CONSTRAINT fk_bookings_facid FOREIGN KEY (facid) REFERENCES cd.facilities(facid),
   
   CONSTRAINT fk_bookings_memid FOREIGN KEY (memid) REFERENCES cd.members(memid)
);
```

И таблица, отслеживающая бронирование объектов. В нем хранится идентификатор объекта, член, который сделал бронирование,
начало бронирования и количество получасовых «слотов», на которые было сделано бронирование.

---

Схематически это выглядит примерно так:
![](./img/schema-horizontal.png)

### Постановка

Составьте список общего количества слотов, забронированных по каждому объекту в месяц в 2012 году. 
В эту версию включите выходные строки, содержащие итоги за все месяцы по объекту и итоги за все месяцы по всем объектам.
Выходная таблица должна состоять из идентификатора объекта, месяца и слотов, отсортированных по идентификатору и месяцу. 
При вычислении агрегированных значений для всех месяцев и всех `facid` возвращайте нулевые значения в столбцах `month` и 
`facid`.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| facid | month | slots |
|-------|-------|-------|
| 0     | 7     | 270   | 
| 0     | 8     | 459   | 
| 0     | NULL  | 729   | 
| 1     | 10    | 100   | 
| 1     | 12    | 300   | 
| 1     | NULL  | 400   | 
| ...   | ...   | ...   | 
| NULL  | NULL  | 10000 |
', null),
(default, '# Задача №2

---

### Контекст

Имеется база данных для недавно созданного загородного клуба. В ней имеется информация о членах этого клуба, объектов для 
отдыха, таких как теннисные корты, и истории бронирования. Помимо прочего, клуб хочет понять, как они могут использовать 
свою информацию для анализа использования/спроса на объекты. __Обратите внимание__: этот набор данных предназначен 
исключительно для интересного набора упражнений, а схема базы данных несовершенна в нескольких аспектах — пожалуйста, не
воспринимайте ее как пример хорошего дизайна.

В БД в схеме `cd` имееются 3 таблицы.  

---

**Таблица `cd.members`**

```postgresql
CREATE TABLE cd.members(
    memid          INTEGER                NOT NULL,
    surname        CHARACTER VARYING(200) NOT NULL,
    firstname      CHARACTER VARYING(200) NOT NULL,
    address        CHARACTER VARYING(300) NOT NULL,
    zipcode        INTEGER                NOT NULL,
    telephone      CHARACTER VARYING(20)  NOT NULL,
    recommendedby  INTEGER,
    joindate       TIMESTAMP              NOT NULL,
    
    CONSTRAINT members_pk PRIMARY KEY (memid),
    
    CONSTRAINT fk_members_recommendedby FOREIGN KEY (recommendedby)
        REFERENCES cd.members(memid) ON DELETE SET NULL
);
```

У каждого участника есть идентификатор (не обязательно последовательный), основная информация об адресе, ссылка на 
участника, который рекомендовал их (если есть), и отметка времени, когда они присоединились.

---

**Таблица `cd.facilities`**

```postgresql
CREATE TABLE cd.facilities(
   facid               INTEGER                NOT NULL, 
   name                CHARACTER VARYING(100) NOT NULL, 
   membercost          NUMERIC                NOT NULL, 
   guestcost           NUMERIC                NOT NULL, 
   initialoutlay       NUMERIC                NOT NULL, 
   monthlymaintenance  NUMERIC                NOT NULL, 
   
   CONSTRAINT facilities_pk PRIMARY KEY (facid)
);
```

В таблице перечислены все доступные для бронирования объекты, которыми располагает загородный клуб. Клуб хранит 
информацию об идентификаторе/имени, стоимости бронирования как членов, так и гостей, первоначальную стоимость строительства объекта и предполагаемые ежемесячные расходы на содержание.

---

**Таблица `cd.bookings`**

```postgresql
CREATE TABLE cd.bookings(
   bookid     INTEGER   NOT NULL, 
   facid      INTEGER   NOT NULL, 
   memid      INTEGER   NOT NULL, 
   starttime  TIMESTAMP NOT NULL,
   slots      INTEGER   NOT NULL,
   
   CONSTRAINT bookings_pk PRIMARY KEY (bookid),
   
   CONSTRAINT fk_bookings_facid FOREIGN KEY (facid) REFERENCES cd.facilities(facid),
   
   CONSTRAINT fk_bookings_memid FOREIGN KEY (memid) REFERENCES cd.members(memid)
);
```

И таблица, отслеживающая бронирование объектов. В нем хранится идентификатор объекта, член, который сделал бронирование,
начало бронирования и количество получасовых «слотов», на которые было сделано бронирование.

---

Схематически это выглядит примерно так:
![](./img/schema-horizontal.png)

### Постановка

Подсчитайте количество рекомендаций, сделанных каждым участником. Упорядочить по идентификатору участника.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| recommendedby | count |
|---------------|-------|
| 1             | 5     |
| 2             | 3     |
| 3             | 1     |
| ...           | ...   |
', null),
(default, '# Задача №7

---

### Контекст

Имеется база данных для недавно созданного загородного клуба. В ней имеется информация о членах этого клуба, объектов для 
отдыха, таких как теннисные корты, и истории бронирования. Помимо прочего, клуб хочет понять, как они могут использовать 
свою информацию для анализа использования/спроса на объекты. __Обратите внимание__: этот набор данных предназначен 
исключительно для интересного набора упражнений, а схема базы данных несовершенна в нескольких аспектах — пожалуйста, не
воспринимайте ее как пример хорошего дизайна.

В БД в схеме `cd` имееются 3 таблицы.  

---

**Таблица `cd.members`**

```postgresql
CREATE TABLE cd.members(
    memid          INTEGER                NOT NULL,
    surname        CHARACTER VARYING(200) NOT NULL,
    firstname      CHARACTER VARYING(200) NOT NULL,
    address        CHARACTER VARYING(300) NOT NULL,
    zipcode        INTEGER                NOT NULL,
    telephone      CHARACTER VARYING(20)  NOT NULL,
    recommendedby  INTEGER,
    joindate       TIMESTAMP              NOT NULL,
    
    CONSTRAINT members_pk PRIMARY KEY (memid),
    
    CONSTRAINT fk_members_recommendedby FOREIGN KEY (recommendedby)
        REFERENCES cd.members(memid) ON DELETE SET NULL
);
```

У каждого участника есть идентификатор (не обязательно последовательный), основная информация об адресе, ссылка на 
участника, который рекомендовал их (если есть), и отметка времени, когда они присоединились.

---

**Таблица `cd.facilities`**

```postgresql
CREATE TABLE cd.facilities(
   facid               INTEGER                NOT NULL, 
   name                CHARACTER VARYING(100) NOT NULL, 
   membercost          NUMERIC                NOT NULL, 
   guestcost           NUMERIC                NOT NULL, 
   initialoutlay       NUMERIC                NOT NULL, 
   monthlymaintenance  NUMERIC                NOT NULL, 
   
   CONSTRAINT facilities_pk PRIMARY KEY (facid)
);
```

В таблице перечислены все доступные для бронирования объекты, которыми располагает загородный клуб. Клуб хранит 
информацию об идентификаторе/имени, стоимости бронирования как членов, так и гостей, первоначальную стоимость строительства объекта и предполагаемые ежемесячные расходы на содержание.

---

**Таблица `cd.bookings`**

```postgresql
CREATE TABLE cd.bookings(
   bookid     INTEGER   NOT NULL, 
   facid      INTEGER   NOT NULL, 
   memid      INTEGER   NOT NULL, 
   starttime  TIMESTAMP NOT NULL,
   slots      INTEGER   NOT NULL,
   
   CONSTRAINT bookings_pk PRIMARY KEY (bookid),
   
   CONSTRAINT fk_bookings_facid FOREIGN KEY (facid) REFERENCES cd.facilities(facid),
   
   CONSTRAINT fk_bookings_memid FOREIGN KEY (memid) REFERENCES cd.members(memid)
);
```

И таблица, отслеживающая бронирование объектов. В нем хранится идентификатор объекта, член, который сделал бронирование,
начало бронирования и количество получасовых «слотов», на которые было сделано бронирование.

---

Схематически это выглядит примерно так:
![](./img/schema-horizontal.png)

### Постановка

Как вывести список всех участников, включая тех, кто их порекомендовал (если есть)? Убедитесь, что результаты 
упорядочены по (фамилии, имени).

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| memfname | memsname | recfname | recsname |
|----------|----------|----------|----------|
| GUEST    | GUEST    | NULL     | NULL     |
| GUEST1   | GUEST1   | GUEST    |  GUEST   |
| ...      | ...      |          |          |
', null),
(default, '# Задача №5

---

### Контекст

Имеется база данных для недавно созданного загородного клуба. В ней имеется информация о членах этого клуба, объектов для 
отдыха, таких как теннисные корты, и истории бронирования. Помимо прочего, клуб хочет понять, как они могут использовать 
свою информацию для анализа использования/спроса на объекты. __Обратите внимание__: этот набор данных предназначен 
исключительно для интересного набора упражнений, а схема базы данных несовершенна в нескольких аспектах — пожалуйста, не
воспринимайте ее как пример хорошего дизайна.

В БД в схеме `cd` имееются 3 таблицы.  

---

**Таблица `cd.members`**

```postgresql
CREATE TABLE cd.members(
    memid          INTEGER                NOT NULL,
    surname        CHARACTER VARYING(200) NOT NULL,
    firstname      CHARACTER VARYING(200) NOT NULL,
    address        CHARACTER VARYING(300) NOT NULL,
    zipcode        INTEGER                NOT NULL,
    telephone      CHARACTER VARYING(20)  NOT NULL,
    recommendedby  INTEGER,
    joindate       TIMESTAMP              NOT NULL,
    
    CONSTRAINT members_pk PRIMARY KEY (memid),
    
    CONSTRAINT fk_members_recommendedby FOREIGN KEY (recommendedby)
        REFERENCES cd.members(memid) ON DELETE SET NULL
);
```

У каждого участника есть идентификатор (не обязательно последовательный), основная информация об адресе, ссылка на 
участника, который рекомендовал их (если есть), и отметка времени, когда они присоединились.

---

**Таблица `cd.facilities`**

```postgresql
CREATE TABLE cd.facilities(
   facid               INTEGER                NOT NULL, 
   name                CHARACTER VARYING(100) NOT NULL, 
   membercost          NUMERIC                NOT NULL, 
   guestcost           NUMERIC                NOT NULL, 
   initialoutlay       NUMERIC                NOT NULL, 
   monthlymaintenance  NUMERIC                NOT NULL, 
   
   CONSTRAINT facilities_pk PRIMARY KEY (facid)
);
```

В таблице перечислены все доступные для бронирования объекты, которыми располагает загородный клуб. Клуб хранит 
информацию об идентификаторе/имени, стоимости бронирования как членов, так и гостей, первоначальную стоимость строительства объекта и предполагаемые ежемесячные расходы на содержание.

---

**Таблица `cd.bookings`**

```postgresql
CREATE TABLE cd.bookings(
   bookid     INTEGER   NOT NULL, 
   facid      INTEGER   NOT NULL, 
   memid      INTEGER   NOT NULL, 
   starttime  TIMESTAMP NOT NULL,
   slots      INTEGER   NOT NULL,
   
   CONSTRAINT bookings_pk PRIMARY KEY (bookid),
   
   CONSTRAINT fk_bookings_facid FOREIGN KEY (facid) REFERENCES cd.facilities(facid),
   
   CONSTRAINT fk_bookings_memid FOREIGN KEY (memid) REFERENCES cd.members(memid)
);
```

И таблица, отслеживающая бронирование объектов. В нем хранится идентификатор объекта, член, который сделал бронирование,
начало бронирования и количество получасовых «слотов», на которые было сделано бронирование.

---

Схематически это выглядит примерно так:
![](./img/schema-horizontal.png)

### Постановка

Верните таблицу имен и идентификаторов каждого участника и их первого бронирования после 1 сентября 2012 года. 
Упорядочьте по идентификатору участника.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| surname  | firstname  | memid | starttime           |
|----------|------------|-------|---------------------|
| GUEST    | GUEST      | 0     | 2012-09-01 08:00:00 |
| ...      | ...        | ...   |                     |


', null),
(default, '# Задача №1

---

### Контекст

В схеме `public` имеется таблица с историей криптовалюты.

```postgresql
CREATE TABLE IF NOT EXISTS coins(
    dt VARCHAR(16),
    avg_price NUMERIC,
    tx_cnt NUMERIC,
    tx_vol NUMERIC,
    active_addr_cnt NUMERIC,
    symbol VARCHAR(8),
    full_nm VARCHAR(128),
    open_price NUMERIC,
    high_price NUMERIC,
    low_price NUMERIC,
    close_price NUMERIC,
    vol NUMERIC,
    market NUMERIC
)
```

Поясним значения хранящиеся в колонках:

* `dt` — дата измерений
* `avg_price` — средняя цена монеты за торговый день в USD
* `tx_cnt` — количество транзакций в сети данной монеты
* `tx_vol` — объем монет переведенных между адресами в сети данной монеты
* `active_addr_cnt` — количество адресов совершавших а данный день транзации в сети данной монеты
* `symbol` — сокращенное название монеты
* `full_nm` — полное название монеты
* `open_price` — цена монеты в начале торгов данного дня
* `high_price` — самая высокая цена данной монеты в течение данного торгового дня
* `low_price` — самая низкая цена данной монеты в течение данного торгового дня
* `close_price` — цена монеты в конце торгов данного дня
* `vol` — объем торгов данной монетой на биржах в данный день
* `market` — капитализация данной монеты в данный день

### Постановка

Найти среднюю, максимальную и минимальную цены по каждой монете.

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| full_name | avg_price | max_price | min_price |
|-----------|-----------|-----------|-----------|
| Bitcoin   | 21352.232 | 60000.0   | 12035.989 |
', null),
(default, '# Задача №6

---

### Контекст

Имеется база данных для недавно созданного загородного клуба. В ней имеется информация о членах этого клуба, объектов для 
отдыха, таких как теннисные корты, и истории бронирования. Помимо прочего, клуб хочет понять, как они могут использовать 
свою информацию для анализа использования/спроса на объекты. __Обратите внимание__: этот набор данных предназначен 
исключительно для интересного набора упражнений, а схема базы данных несовершенна в нескольких аспектах — пожалуйста, не
воспринимайте ее как пример хорошего дизайна.

В БД в схеме `cd` имееются 3 таблицы.  

---

**Таблица `cd.members`**

```postgresql
CREATE TABLE cd.members(
    memid          INTEGER                NOT NULL,
    surname        CHARACTER VARYING(200) NOT NULL,
    firstname      CHARACTER VARYING(200) NOT NULL,
    address        CHARACTER VARYING(300) NOT NULL,
    zipcode        INTEGER                NOT NULL,
    telephone      CHARACTER VARYING(20)  NOT NULL,
    recommendedby  INTEGER,
    joindate       TIMESTAMP              NOT NULL,
    
    CONSTRAINT members_pk PRIMARY KEY (memid),
    
    CONSTRAINT fk_members_recommendedby FOREIGN KEY (recommendedby)
        REFERENCES cd.members(memid) ON DELETE SET NULL
);
```

У каждого участника есть идентификатор (не обязательно последовательный), основная информация об адресе, ссылка на 
участника, который рекомендовал их (если есть), и отметка времени, когда они присоединились.

---

**Таблица `cd.facilities`**

```postgresql
CREATE TABLE cd.facilities(
   facid               INTEGER                NOT NULL, 
   name                CHARACTER VARYING(100) NOT NULL, 
   membercost          NUMERIC                NOT NULL, 
   guestcost           NUMERIC                NOT NULL, 
   initialoutlay       NUMERIC                NOT NULL, 
   monthlymaintenance  NUMERIC                NOT NULL, 
   
   CONSTRAINT facilities_pk PRIMARY KEY (facid)
);
```

В таблице перечислены все доступные для бронирования объекты, которыми располагает загородный клуб. Клуб хранит 
информацию об идентификаторе/имени, стоимости бронирования как членов, так и гостей, первоначальную стоимость строительства объекта и предполагаемые ежемесячные расходы на содержание.

---

**Таблица `cd.bookings`**

```postgresql
CREATE TABLE cd.bookings(
   bookid     INTEGER   NOT NULL, 
   facid      INTEGER   NOT NULL, 
   memid      INTEGER   NOT NULL, 
   starttime  TIMESTAMP NOT NULL,
   slots      INTEGER   NOT NULL,
   
   CONSTRAINT bookings_pk PRIMARY KEY (bookid),
   
   CONSTRAINT fk_bookings_facid FOREIGN KEY (facid) REFERENCES cd.facilities(facid),
   
   CONSTRAINT fk_bookings_memid FOREIGN KEY (memid) REFERENCES cd.members(memid)
);
```

И таблица, отслеживающая бронирование объектов. В нем хранится идентификатор объекта, член, который сделал бронирование,
начало бронирования и количество получасовых «слотов», на которые было сделано бронирование.

---

Схематически это выглядит примерно так:
![](./img/schema-horizontal.png)

### Постановка

Как вывести список всех участников, которые порекомендовали другого участника? Убедитесь, что в списке нет дубликатов и 
что результаты упорядочены по (фамилии, имени).

### Ожидаемый формат ответа

Ваш запрос должен возвращать таблицу формата:

| firstname | surname    |
|-----------|------------|
| GUEST     | GUEST      |
| ...       | ...        |


', null);
