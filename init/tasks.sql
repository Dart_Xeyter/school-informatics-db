create table task(
    contest_num int references contest(number)
        on update cascade on delete cascade,
    number char(2),
    statement_id serial references statement(id)
        on delete set null,
    name text,
    TL smallint not null default 2000
        check(TL >= 100 and TL <= 30000), -- in ms

    primary key (contest_num, number)
);

-- insert into task values (003, 'ZA', 1, 'asdf');
-- insert into task values (003, 'A', 13);
-- insert into task values (004, 'ZA', null, null, 179);
--
-- select * from task;
