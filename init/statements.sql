create table statement(
    id serial primary key,
    cur text not null,
    prev text
);

-- insert into statement values (1, 'mama vskf hfve');
-- insert into statement values (13, 'gogo');
-- insert into statement values (2, 'mama vskf hfve', 'papa');
--
-- select * from statement;
