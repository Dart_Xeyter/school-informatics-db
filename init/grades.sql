create table grade(
    id serial primary key,
    pupil_id serial not null references pupil(id)
        on delete cascade,
    score smallint not null,
    contest_num int references contest(number)
        on update cascade on delete set null,
    date_from date default now()::date,
    date_to date not null default '3000-01-01',

    check(date_from <= date_to)
);

-- insert into grade values (default, 3, 5, null, null, default);
-- insert into grade values (default, 1, 3, 3, default, '2999-12-13');
-- insert into grade values (default, 3, 5, 001, '2024-02-02', default);
--
-- select * from grade;
