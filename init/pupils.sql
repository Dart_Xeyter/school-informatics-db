create table pupil (
    id serial primary key,
    name text not null,
    surname text not null,
    gender char check (gender in ('М', 'Ж'))
);

-- insert into pupil values (1, '32', 'ss');
-- insert into pupil values (2, '32', 's');
-- insert into pupil values (3, '3233', 'mamama', 'Ж');
--
-- select * from pupil;
