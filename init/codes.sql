create table code(
    id serial primary key,
    language char(20),
    program text not null
);

-- insert into code values (1, null, 'asdf');
-- insert into code values (13, 'Microsoft Power Fx', 'aaa');
-- insert into code values (2, 'python 3.12', 'print(3)');
--
-- select * from code;
