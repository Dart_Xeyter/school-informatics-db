drop table if exists statement cascade;
drop table if exists code cascade;
drop table if exists contest cascade;
drop table if exists pupil cascade;
drop table if exists task cascade;
drop table if exists submission cascade;
drop table if exists grade cascade;
