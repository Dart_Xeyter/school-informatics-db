create table submission(
    id serial primary key references code(id)
        on delete cascade,
    contest_num int not null,
    task_num char(2) not null,
    pupil_id serial not null references pupil(id)
        on delete cascade,
    result char(3) not null,
    time smallint check(time >= 0),

    foreign key (contest_num, task_num)
        references task(contest_num, number)
        on update cascade on delete cascade
);

-- insert into submission values (1, 003, 'A', 1, 'WA');
-- insert into submission values (2, 003, 'A', 1, 'TL', 32179);
-- insert into submission values (13, 003, 'ZA', 3, 'OK', 0);
--
-- select * from submission;
