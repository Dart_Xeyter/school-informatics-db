create table contest(
    number int primary key check (number >= 0),
    topic text,
    deadline date
);

-- insert into contest values (003, 'papa');
-- insert into contest values (004, null, '2022-02-22');
-- insert into contest values (001);
--
-- select * from contest;
