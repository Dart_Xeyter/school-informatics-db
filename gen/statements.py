from generate import generate
import os
name = os.path.basename(__file__)


def gen_insert():
    for dir in os.scandir('../statement_examples'):
        for file in os.scandir(dir):
            statement = open(file.path, 'r', encoding='utf-8').read()
            statement = statement.replace("'", "''")
            yield 'default', f"'{statement}'", 'null'
    os.scandir('../statement_examples').close()


generate(name, gen_insert)
