from generate import generate
from random import *
import os
name = os.path.basename(__file__)
res = ["OK", "TL", "WA", "RE"]


def gen_insert():
    n = 97
    for num in range(n):
        pupil_id = randint(1, 40)
        score = randint(2, 5)
        contest_num = randint(1, 10)
        date = 'default' if choice([True, False]) else 'null'
        yield 'default', pupil_id, score, contest_num, date, 'default'


generate(name, gen_insert)
