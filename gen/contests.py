from generate import generate
from datetime import date
from random import *
import os
name = os.path.basename(__file__)
A = ["Весёлые массивы", "Чёрная пятница", "Очередная задача на синхрофазотрон", 'null']


def gen_insert():
    n = 43
    for number in range(1, n+1):
        text = choice(A)
        text = text if text == 'null' else f"'{text}'"
        deadline = date(randint(1984, 2048), randint(1, 12), randint(1, 23))
        yield number, text, f"'{deadline}'"


generate(name, gen_insert)
