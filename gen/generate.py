def prepare(name):
    name = name.replace('.py', '.sql')
    way = "../insert/" + str(name)
    name = name[:name.find('.') - 1]
    return name, way


def gen(name, way, gen_insert):
    with open(way, 'w', encoding='utf-8') as file:
        file.write(f"insert into {name} values\n")
        for data in gen_insert():
            file.write(f"({', '.join(map(str, data))}),\n")


def fix(way):
    with open(way, 'r', encoding='utf-8') as file:
        text = file.read()
    text = text.strip()[:-1] + ';\n'
    with open(way, 'w', encoding='utf-8') as file:
        file.write(text)


def generate(name, gen_insert):
    name, way = prepare(name)
    gen(name, way, gen_insert)
    fix(way)
