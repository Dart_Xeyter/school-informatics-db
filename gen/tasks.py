from generate import generate
from random import *
import os
name = os.path.basename(__file__)
A = ["Весёлые массивы", "Чёрная пятница", "Очередная задача на синхрофазотрон", 'null']


def gen_insert():
    n, m = 13, 17
    for num in range(1, n+1):
        for q in range(m):
            number = chr(ord('A')+q)
            contest_num = num
            statement_id = randint(1, 15)
            text = choice(A)
            text = text if text == 'null' else f"'{text}'"
            tl = randint(1, 300)*100
            yield contest_num, f"'{number}'", statement_id, text, tl


generate(name, gen_insert)
