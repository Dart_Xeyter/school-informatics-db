from generate import generate
import os
name = os.path.basename(__file__)


def gen_insert():
    for dir in os.scandir('../code_examples'):
        for file in os.scandir(dir):
            code = open(file.path, 'r', encoding='utf-8').read()
            code = code.replace("'", "''")
            lang = "g++" if code.find('#include') != -1 else "python"
            yield 'default', f"'{lang}'", f"'{code}'"
    os.scandir('../code_examples').close()


generate(name, gen_insert)
