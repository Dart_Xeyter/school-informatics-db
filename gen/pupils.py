from generate import generate
from random import *
import os
name = os.path.basename(__file__)
A = ["Андрей", "Алексей", "Борис", "Всеволод", "Мишель", "Юрий", "Актимер", "Игнатий", "Пин", "Владимир", "Гомер", "Женя"]
B = ["Инесса", "Виолетта", "Юлия", "Алла", "Изабелла", "Груня", "Джакарта", "Женя", "Нюша", "Ася", "Евфросиния"]
sur = ["Комарова", "Люлько", "Кольчина", "Межерева", "Ким", "Цой", "Крепова", "Пушкина", "Евдокимова", "Савватеева", "Пырэу"]


def gen_insert():
    n = 47
    for _ in range(n):
        gender = choice([False, True])
        is_null = choice([False, True])
        first = choice(A if gender else B)
        second = choice(sur)
        second = second[:-1] if gender else second
        gender = ('М' if gender else 'Ж') if not is_null else None
        gender = 'null' if gender is None else f"'{gender}'"
        yield 'default', f"'{first}'", f"'{second}'", gender


generate(name, gen_insert)
