from generate import generate
import codes
from random import *
import os
name = os.path.basename(__file__)
res = ["OK", "TL", "WA", "RE"]


def gen_insert():
    n = sum(1 for _ in codes.gen_insert())
    for num in range(n):
        number = chr(ord('A')+randint(0, 15))
        contest_num = randint(1, 10)
        pupil_id = randint(1, 40)
        result = choice(res)
        time = randint(0, 300)*100
        yield 'default', contest_num, f"'{number}'", pupil_id, f"'{result}'", time


generate(name, gen_insert)
