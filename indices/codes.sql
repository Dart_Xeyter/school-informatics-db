create index if not exists code_scanner on code using gin(to_tsvector('english', program));
set enable_seqscan = false;

explain select program from code where id < 10;

explain (analyse) select language from code
where to_tsvector('english', program) @@ to_tsquery('english', 'include');
-- Применяется, когда нужно быстро искать в программах какие-то паттерны
