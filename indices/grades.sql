create index if not exists grade_pupil on grade using hash(pupil_id);
create index if not exists grade_date on grade using btree(date_from);
create index if not exists grade_contest on grade using btree(contest_num);
set enable_seqscan = false;

explain (analyse) select * from grade where pupil_id = 13;
-- В состоянии мгновенно посмотреть оценки конкретного ученика

explain (analyse) select * from grade where grade.date_from <= '2024-01-01';
-- А также отметки за интересующие даты

explain (analyse) select * from grade where contest_num < 3;
-- И по выделенным контестам
