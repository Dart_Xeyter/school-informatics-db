create index if not exists sub_verdict on submission using hash(result);
create index if not exists sub_pupil on submission using hash(pupil_id);
create index if not exists sub_contest_num on submission using btree(contest_num);
set enable_seqscan = false;

explain (analyse) select count(*) from submission where result = 'IG';
-- Можем быстро находить посылки с нужным статусом

explain (analyse) select count(*) from submission where pupil_id = 8;
-- От конкретного человека

explain (analyse) select count(*) from submission where 5 <= contest_num and contest_num <= 7;
-- И пробегать по сдачам в актуальных контестах
