create or replace function chg_TL() returns trigger as $$
begin
    update submission set result = 'TL' where time > new.TL and
        contest_num = new.contest_num and task_num = new.number;
    update submission set result = 'IG' where time <= new.TL and result = 'TL' and
        contest_num = new.contest_num and task_num = new.number;
    return new;
end
$$ language plpgsql;

create or replace trigger change_TL after update of TL on task
for each row execute function chg_TL();

-- update task set TL = 22000 where contest_num = 8 and number = 'M';
-- select * from task;
-- select * from submission;
