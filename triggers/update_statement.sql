create or replace function upd_statement() returns trigger as $$
begin
    new.prev = old.cur;
    return new;
end
$$ language plpgsql;

create or replace trigger update_statement before update of cur on statement
for each row execute function upd_statement();

-- update statement set cur = 'propaaaaaaaro' where id = 138;
-- select * from statement;
