create or replace function check_sub() returns trigger as $$
declare
    TL int = (select TL from task
              where contest_num = new.contest_num and number = new.task_num);
begin
    if (TL < new.time) != (new.result = 'TL') then
        raise exception 'Incorrect result!';
    end if;
    if new.result = 'OK' then
        update submission set result = 'IG'
        where result = 'OK' and pupil_id = new.pupil_id and
        contest_num = new.contest_num and task_num = new.task_num;
    end if;
    return new;
end
$$ language plpgsql;

create or replace trigger check_submission before insert on submission
for each row execute function check_sub();

-- insert into code values (120, null, 'print(179)');
-- insert into submission values (120, 2, 'I', 15, 'OK', 10000);
--
-- select * from task;
-- select * from submission;
