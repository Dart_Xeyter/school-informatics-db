select code.language, round(avg(length(code.program)), 2)
from code group by code.language;
-- Насколько длины программ различаются в различных языках

select gender, count(*)
from pupil inner join submission on pupil.id = submission.pupil_id
group by gender;
-- Сколько посылок сделано мальчиками, сколько девочками, а сколько неизвестно

select pupil.name, pupil.surname, count(*)
from pupil inner join submission on pupil.id = submission.pupil_id
group by pupil.id order by count(*) desc;
-- Список школьников, отсортированный по кол-ву посылок

select contest_num, round(avg(score), 2)
from grade where date_to > now()::date
group by contest_num having count(*) >= 10
order by contest_num;
-- Средний балл по контестам, имеющим хотя бы 10 актуальных оценок

select contest_num as contest, task_num as task, min(time)/1000.0 as time
from submission where result = 'OK'
group by (contest_num, task_num) order by (contest_num, task_num);
-- Минимальное время выполнения корректных посылок по задачам

select pupil.name, surname
from pupil inner join grade on pupil.id = grade.pupil_id
where date_to > now()::date
group by pupil.id having min(score) >= 3;
-- Список школьников, не имеющих долгов (двоек)

create table honor_roll as
select id, name, surname from pupil
where 5 = all(select score from grade where pupil_id = pupil.id);
select * from honor_roll;
-- Только круглые отличники

select result, count(*) from submission
where pupil_id in (select id from honor_roll)
group by result;
-- Статистика вердиктов посылок среди отличников

with slacker as (
select id from pupil where not exists(
select * from submission where pupil_id = pupil.id)
)
select avg(score) from grade where pupil_id in (select id from slacker);
-- Средний балл по бездельникам, не сделавшим ни одной посылки

select number, name, TL, rank() over (order by TL)
from task where contest_num = 3;
-- Задачи 3-го контеста, отранжированные по ограничению по времени

select distinct topic, min(deadline) over
(partition by topic) as date from contest
where deadline > now()::date order by date;
-- Ближайшие дедлайны по контестам каждой из тем
